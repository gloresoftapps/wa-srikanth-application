import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { tap, map } from 'rxjs/operators';

import { AuthResponse, LoginResponse, UserType } from './../../models';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private API_BASE = 'http://localhost:8080/weatherforecast/api/accounts/v1/';
  private isLoggedIn = new BehaviorSubject<boolean>(false);
  private isAdmin = new BehaviorSubject<boolean>(false);
  isLoggedIn$ = this.isLoggedIn.asObservable();
  isAdmin$ = this.isAdmin.asObservable();

  constructor(private readonly http: HttpClient) {
    this.isLoggedIn.next(sessionStorage.getItem('token') ? true : false);
  }

  login(username: string, password: string) {
    return this.http
      .post<LoginResponse>(`${this.API_BASE}login`, {
        username,
        password,
      })
      .pipe(
        tap((response: LoginResponse) => {
          sessionStorage.setItem('token', JSON.stringify(response));
          this.isLoggedIn.next(true);
          this.isAdmin.next(response.type === UserType.ADMIN);
        }),
        map((response: LoginResponse) => 'User logged in successfully')
      );
  }

  register(
    username: string,
    password: string,
    dateOfBirth: string,
    type: string
  ) {
    return this.http
      .post<AuthResponse>(`${this.API_BASE}register`, {
        username,
        password,
        dateOfBirth,
        type,
      })
      .pipe(
        map(
          (response: AuthResponse) =>
            (response && response.message) || 'User logged in successfully'
        )
      );
  }

  logout() {
    sessionStorage.removeItem('token');
    this.isLoggedIn.next(false);
    this.isAdmin.next(false);
  }

  isAuthenticated(propertyToRead = 'customerId') {
    const loggedInUser = JSON.parse(sessionStorage.getItem('token'));
    return loggedInUser && loggedInUser[propertyToRead];
  }
}

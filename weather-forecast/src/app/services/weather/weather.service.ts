import { AuthService } from './../auth/auth.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { WeatherHistoryItem, SearchBy } from './../../models';
import { switchMap, tap, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class WeatherService {
  private API_BASE = `http://localhost:8080/weatherforecast/api/weather/v1/`;
  private LIST_BY_CITY = `${this.API_BASE}search/customers?city=`;
  private LIST_BY_USER = `${this.API_BASE}search/cities?customerId=`;

  constructor(
    private readonly http: HttpClient,
    private readonly authService: AuthService
  ) {}

  updateWeatherHistoryItem({
    description,
    weatherId,
    sunRise,
    sunSet,
    temperature,
  }: WeatherHistoryItem) {
    return this.http
      .put(`${this.API_BASE}update/${weatherId}`, {
        description,
        sunRise,
        sunSet,
        temperature,
      })
      .pipe(switchMap((res) => this.getWeatherSearchHistoryForUser()));
  }

  getAdminSearchResults(searchBy: SearchBy, searchQuery: string) {
    return this.http
      .get(
        `${
          searchBy === SearchBy.CITY ? this.LIST_BY_CITY : this.LIST_BY_USER
        }${searchQuery}`
      )
      .pipe(
        map((res: Array<any>) =>
          res.map(
            (item) => item[searchBy === SearchBy.CITY ? 'username' : 'city']
          )
        ),
        tap((res) => console.log('Got the resul as : ', res))
      );
  }

  getWeatherSearchHistoryForUser() {
    return this.http.get<Array<WeatherHistoryItem>>(
      `${this.API_BASE}history/${this.authService.isAuthenticated()}`
    );
  }

  getWeatherForCity(cityName: string) {
    return this.http.post<WeatherHistoryItem>(
      `${this.API_BASE}search/${this.authService.isAuthenticated()}`,
      {
        city: cityName,
        name: this.authService.isAuthenticated('username'),
      }
    );
  }

  deleteHistoryItem(weatherId) {
    return this.http
      .post(`${this.API_BASE}delete/${weatherId}`, {
        customerId: this.authService.isAuthenticated(),
      })
      .pipe(
        tap((res) => console.log(res)),
        switchMap((res) => this.getWeatherSearchHistoryForUser())
      );
  }

  deleteBulkHistory(weathers: Array<string>) {
    return this.http
      .post(`${this.API_BASE}delete/bulk`, {
        weathers,
        customerId: this.authService.isAuthenticated(),
      })
      .pipe(
        tap((res) => console.log(res)),
        switchMap((res) => this.getWeatherSearchHistoryForUser())
      );
  }
}

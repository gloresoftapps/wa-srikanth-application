export interface AuthResponse {
  id?: string;
  message: string;
  success: boolean;
}

export enum UserType {
  ADMIN = 'admin',
  USER = 'basic',
}

export enum SearchBy {
  CITY = 'city',
  USER = 'customerId',
}

export interface LoginResponse {
  customerId: string;
  username: string;
  type: UserType;
}

export interface WeatherHistoryItem {
  weatherId: string;
  city: string;
  description: string;
  sunRise: string | Date;
  sunSet: string | Date;
  temperature: number;
  minTemperature: number;
  maxTemperature: number;
  customers: null;
}

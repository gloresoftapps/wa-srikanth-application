import { AdminUserGuard } from './guards/admin-user/admin-user.guard';
import { UserActivitiesComponent } from './components/user-activities/user-activities.component';
import { WeatherSearchComponent } from './components/weather-search/weather-search.component';
import { HistoryComponent } from './components/history/history.component';
import { AuthGuard } from './guards/auth/auth.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'register',
    component: RegisterComponent,
  },
  {
    path: 'home',
    component: WeatherSearchComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'history',
    component: HistoryComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'user-activities',
    component: UserActivitiesComponent,
    canActivate: [AuthGuard, AdminUserGuard],
  },
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}

import { AuthResponse } from './../../models';
import { AuthService } from './../../services/auth/auth.service';
import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent {
  username: string;
  password: string;
  dateOfBirth: string;
  type: string;

  constructor(
    private readonly authService: AuthService,
    private readonly router: Router
  ) {}

  register() {
    this.authService
      .register(this.username, this.password, this.dateOfBirth, this.type)
      .subscribe((responseMessage: string) => {
        alert(responseMessage);
        this.router.navigateByUrl('/login');
      });
  }
}

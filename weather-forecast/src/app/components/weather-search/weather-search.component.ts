import { WeatherService } from './../../services/weather/weather.service';
import { WeatherHistoryItem } from './../../models';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-weather-search',
  templateUrl: './weather-search.component.html',
  styleUrls: ['./weather-search.component.scss'],
})
export class WeatherSearchComponent {
  cityName: string;
  weatherDetails$: Observable<WeatherHistoryItem>;

  constructor(private readonly weatherService: WeatherService) {}

  getWeatherDetails() {
    this.weatherDetails$ = this.weatherService.getWeatherForCity(this.cityName);
  }
}

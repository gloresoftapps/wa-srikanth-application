import { WeatherService } from './../../services/weather/weather.service';
import { WeatherHistoryItem } from './../../models';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss'],
})
export class HistoryComponent {
  weatherHistoryRecords$ = this.weatherService.getWeatherSearchHistoryForUser();
  selectedItemsToDelete = new Set();
  itemToBeEdited: WeatherHistoryItem;

  constructor(private readonly weatherService: WeatherService) {}

  edit(historyItem: WeatherHistoryItem) {
    this.itemToBeEdited = {
      ...historyItem,
    };
  }

  cancel() {
    this.itemToBeEdited = null;
  }

  update() {
    this.weatherHistoryRecords$ = this.weatherService
      .updateWeatherHistoryItem(this.itemToBeEdited)
      .pipe(tap(() => (this.itemToBeEdited = null)));
  }

  delete(historyItem: WeatherHistoryItem) {
    if (confirm('Are you sure you want to delete this item?')) {
      this.weatherHistoryRecords$ = this.weatherService.deleteHistoryItem(
        historyItem.weatherId
      );
    }
  }

  handleCheckChange($event, selectedWeatherId) {
    if ($event.target.checked) {
      this.selectedItemsToDelete.add(selectedWeatherId);
    } else {
      this.selectedItemsToDelete.delete(selectedWeatherId);
    }
  }

  deleteBulkHistory() {
    if (confirm('Are you sure you want to delete the selected item(s)?')) {
      let itemsToDelete = [];
      this.selectedItemsToDelete.forEach((value) => itemsToDelete.push(value));
      if (itemsToDelete.length)
        this.weatherHistoryRecords$ = this.weatherService.deleteBulkHistory(
          itemsToDelete
        );
    }
  }
}

import { WeatherService } from './../../services/weather/weather.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { SearchBy } from 'src/app/models';

@Component({
  selector: 'app-user-activities',
  templateUrl: './user-activities.component.html',
  styleUrls: ['./user-activities.component.scss'],
})
export class UserActivitiesComponent {
  searchBy: string = 'city';
  searchQuery: string;
  results$: Observable<Array<any>>;

  constructor(private readonly weatherService: WeatherService) {}

  onSearch() {
    this.results$ = this.weatherService.getAdminSearchResults(
      this.searchBy as SearchBy,
      this.searchQuery
    );
  }
}

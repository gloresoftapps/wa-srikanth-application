# WeatherForecast

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.2.0.

## Setup on Local

- Clone the git repo from `https://bitbucket.org/abhijitingle/wa-srikanth-application`
- Once the git repo is cloned, import the angular project from folder named weather-forecast in VSCode editor. Here is a reference to article that was referred to setup Local Environment - `https://codeburst.io/getting-started-with-angular-f92236eaed70?gi=a21f7ed62c6`. This will let you install [NodeJS](https://nodejs.org/en/download/), [VSCode](https://code.visualstudio.com/download), [AngularCLI](https://cli.angular.io/).
- Navigate to the Project Folder.
- Install the dependencies by running `npm i`. This installs all the dependencies that are present inside of the `package.json` file.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Code structure

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module <building-block-name>`.

Running this command will only generate a folder in case of Components and Modules. It won't generate a folder in case of other things like guards, services, directives etc.

In case you want it to generate a folder while creating these other things, you can run a command like:

`ng g s my-service/my-service` - This will generate a service called `my-service` inside a folder named `my-service`. Two files would be generated. `my-service.service.ts` and `my-service.service.spec.ts`.

You can do the same things for directives, pipes, guards, etc.

## Install new Dependencies

Run `npm i <package-name>` to install that package as a dependency. When in doubt of the `package-name`, just look it up on the [NPM Registry](https://www.npmjs.com)

## Folder Structure

- All the components are inside the `components` Folder
- All the services are inside the `services` Folder
- All the guards are inside the `guards` Folder

## Future Enhancements

- Make Separate Modules for Different Pages that we have.
- Lazy Load these modules on demand.
- Add Unit Tests.
- Add NgRx for State Management.
- Status messages for better intuitive user experience

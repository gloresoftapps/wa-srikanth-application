# WeatherForecast

The frontend project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.2.0.

## Setup on Local

- Clone the git repo from `https://bitbucket.org/abhijitingle/wa-srikanth-application`
- Once the git repo is cloned, import the angular project from folder named weather-forecast in VSCode editor. Here is a reference to article that was referred to setup Local Environment - `https://codeburst.io/getting-started-with-angular-f92236eaed70?gi=a21f7ed62c6`. This will let you install [NodeJS](https://nodejs.org/en/download/), [VSCode](https://code.visualstudio.com/download), [AngularCLI](https://cli.angular.io/).
- Navigate to the Project Folder.
- Install the dependencies by running `npm i`. This installs all the dependencies that are present inside of the `package.json` file.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Code structure

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module <building-block-name>`.

Running this command will only generate a folder in case of Components and Modules. It won't generate a folder in case of other things like guards, services, directives etc.

In case you want it to generate a folder while creating these other things, you can run a command like:

`ng g s my-service/my-service` - This will generate a service called `my-service` inside a folder named `my-service`. Two files would be generated. `my-service.service.ts` and `my-service.service.spec.ts`.

You can do the same things for directives, pipes, guards, etc.

## Install new Dependencies

Run `npm i <package-name>` to install that package as a dependency. When in doubt of the `package-name`, just look it up on the [NPM Registry](https://www.npmjs.com)

## Folder Structure

- All the components are inside the `components` Folder
- All the services are inside the `services` Folder
- All the guards are inside the `guards` Folder

## Future Enhancements

- Make Separate Modules for Different Pages that we have.
- Lazy Load these modules on demand.
- Add Unit Tests.
- Add NgRx for State Management.
- Status,Loading and Validation specific messages for better intuitive user experience

# Weatherforecast-Springboot

A spring boot web service to search and work with weather forecast and its information

# Technologies used
- Java 8
- Spring Boot 2.3.4
- REST
- Hibernate
- Postgres
- Maven 3
- Junit 4
- Swagger
- OpenPojo
- Sonar and Jacoco
- Spring Tool Suite 4 (IDE used for development)
- Postman (To validate API's)


## Project Setup
- Clone the project from following github link : `https://bitbucket.org/abhijitingle/wa-srikanth-application`
- Once the git repo is cloned, import the project weatherforecast-springboot in an java editor preferable Spring Tool Suite 4
- Install the following in your local environment 
  - Java 8 (`https://www.oracle.com/in/java/technologies/javase/javase-jdk8-downloads.html`)
  - Postgres database (`https://www.postgresql.org/download/windows/`)
  - pgAdmin(`https://www.pgadmin.org/download/pgadmin-4-windows/`)
  - Spring Tool Suite(STS) (`https://spring.io/tools`)
  - Sonar latest free commnuity version (`https://www.sonarqube.org/downloads/`)
  - Postman from following link (`https://www.postman.com/downloads/`)
  - Maven, Sonarlint(Optional), EclEmma Java Code Coverage(Optional), Moreunit(Optional) from Eclipse Marketplace in STS
  - Download Maven from following link (`http://maven.apache.org/download.cgi`)
- Import the project in STS and then run Maven build.
- Navigate to src/main/resources folder and copy the DDL queries from weatherforecast_ddl_1/.sql file and create the tables in Postgres database
  using pgAdmin client. For testing in our local environment we are using the default database named postgres and schema
  named public. Additionally we have added the database user information in HibernateConfig.java file.
- Now we are ready to validate the API's in Postman. Import json file inside weathereforecast-springboot folder named Weatherforecast.postman_collection.json
- The frontend has been integrated with Angular app that can be utilized for validating with frontend
- Once the API and Frontend is up and running in following ports 8080 and 4200 respectively we shall start validating the application.
- To check code coverage right click project and run maven build. The code coverage is generate target/site/jacoco/index.html file.
  Open the file in a browser to view the code coverage 
- Run the sonar analysis using mvn sonar:sonar command once you start the sonar server

## Web Services
The following web services have been developed as part of the implementation. The endpoints mentioned are referring to local environment. 
For reading the latest weather updates, OpenWeatherMap API has been integrated for search weather functionality.
A word document is added in the following link that contains sample API endpoint, request and responses for the 
web services. Please refer the document named Weatherforecast_Backend.

1. Register Customer.
2. Login Customer.
3. Search Weather By City Name.
4. Update Weather Details such as description, temperature, sunRise and sunSet.
5. Delete Weather
6. Delete Bulk Weather
7. Read Weather History for logged in Customer.
8. Search Customers By City Name
9. Search Cities By Customer Name 

API documentation is integrated with Swagger and the Swagger url can be accessed here : `http://localhost:8080/weatherforecast/api/swagger-ui.html`
Postman collection for API's can be found in the root folder of the project i.e in following link. Once the project is downloaded in your local, you can setup Postman and then import the json file to validate the API's.

## Further Enhancements/Improvements
- Improve coverage for code
- Add more unit and integration test cases
- Exception handling and logger improvements
- Devops implementation
- Configure secure and secrets data such as Database information, API token in secrets once the Devops pipeline is setup
- Implement autenthication using an authentication service provider such as OAuth, Okta and so forth 
- Migrate from Junit 4 to Junit 5 
- Project Lombok for getters and setters management
- Flyway for database migration
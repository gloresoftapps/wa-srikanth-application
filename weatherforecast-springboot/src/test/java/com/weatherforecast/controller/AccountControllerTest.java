package com.weatherforecast.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.weatherforecast.dto.request.AccountRequest;
import com.weatherforecast.dto.request.CustomerRequest;
import com.weatherforecast.dto.response.AppResponse;
import com.weatherforecast.exception.BaseException;
import com.weatherforecast.service.IAccountService;

/**
 * The Class AccountControllerTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class AccountControllerTest {
	
	/** The account request. */
	@Mock	
	private AccountRequest accountRequest;
	
	/** The account service. */
	@Mock
	private IAccountService accountService;
	
	/** The account controller. */
	@InjectMocks
	private AccountController accountController;
	
	/** The app response. */
	@Mock
	private AppResponse appResponse;
	
	private MockMvc mvc;
	
	private MvcResult result;
	
	/**
	 * Sets the up.
	 */
	@Before
	public void setUp() {
		this.mvc = MockMvcBuilders.standaloneSetup(accountController).build();
	}
	
	/**
	 * Test register account MVC.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testRegisterAccountMVC() throws Exception {
		String reqJson = "{\"username\" : \"testuser@test.com\",\"password\" : \"testpwd\",\"dateOfBirth\" : \"2020-01-01\",\"type\" : \"testtype\"}";
		this.mvc.perform(post("/accounts/v1/register").contentType("application/json")
				.content(reqJson)).andExpect(status().isCreated());
	}
	
	/**
	 * Test login customer MVC.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testLoginCustomerMVC() throws Exception {
		String reqJson = "{\"username\": \"test@test.com\",\"password\": \"testpwd\"}";
		result = this.mvc.perform(post("/accounts/v1/login").contentType("application/json")
				.content(reqJson)).andReturn();
		assertThat(result).isNotNull();
	}
	
	/**
	 * Test register account.
	 *
	 * @throws BaseException the base exception
	 */
	@Test
	 public void testRegisterAccount() throws BaseException {				
		accountController.registerAccount(accountRequest);		
		assertThat(appResponse).isNotNull();
	}
	
	/**
	 * Test login customer.
	 *
	 * @throws BaseException the base exception
	 */
	@Test
	public void testLoginCustomer() throws BaseException {
		CustomerRequest request = new CustomerRequest();
		request.setUsername("testuser");
		request.setPassword("testpwd");
		assertThat(accountController.loginCustomer(request)).isNotNull();		
	}
}

package com.weatherforecast.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import com.weatherforecast.dto.request.DeleteRequest;
import com.weatherforecast.dto.request.SearchRequest;
import com.weatherforecast.dto.request.WeatherRequest;
import com.weatherforecast.dto.response.AppResponse;
import com.weatherforecast.dto.response.CustomerResponse;
import com.weatherforecast.dto.response.WeatherResponse;
import com.weatherforecast.exception.BaseException;
import com.weatherforecast.service.IWeatherService;
import com.weatherforecast.validator.WeatherValidator;

/**
 * The Class WeatherControllerTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class WeatherControllerTest {

	/** The controller. */
	@InjectMocks
	private WeatherController controller;

	/** The weather service. */
	@Mock
	private IWeatherService weatherService;

	/** The app response. */
	@Mock
	private AppResponse appResponse;

	/** The weather request. */
	private WeatherRequest weatherRequest;

	/** The list weather request. */
	private List<WeatherRequest> listWeatherRequest;

	/** The list weather response. */
	@Mock
	private List<WeatherResponse> listWeatherResponse;

	/** The weather response. */
	@Mock
	private WeatherResponse weatherResponse;

	/** The list customer response. */
	@Mock
	private List<CustomerResponse> listCustomerResponse;

	/** The delete request. */
	@Mock
	private DeleteRequest deleteRequest;

	/** The search request. */
	@Mock
	private SearchRequest searchRequest;

	/** The validator. */
	@Mock
	private WeatherValidator validator;

	/**
	 * Sets the up.
	 */
	@Before
	public void setUp() {
		weatherRequest = new WeatherRequest();
		weatherRequest.setCity("testcity");
		weatherRequest.setCustomerId("123");
		weatherRequest.setDescription("testdesc");
		weatherRequest.setMaxTemperature(new BigDecimal(12.34));
		weatherRequest.setMinTemperature(new BigDecimal(11.23));
		weatherRequest.setSunRise("2020-11-09T09:36:26");
		weatherRequest.setSunSet("2020-11-09T18:36:26");
		weatherRequest.setTimezone(9600);
		weatherRequest.setTemperature(new BigDecimal(12.54));
	}

	/**
	 * Test read weather history.
	 *
	 * @throws BaseException the base exception
	 */
	@Test
	public void testReadWeatherHistory() throws BaseException {
		when(weatherService.readWeatherHistory(Mockito.anyString())).thenReturn(listWeatherResponse);
		ResponseEntity<List<WeatherResponse>> response = controller.readWeatherHistory("656");
		assertEquals(listWeatherResponse, response.getBody());
	}

	/**
	 * Test update weather.
	 *
	 * @throws BaseException the base exception
	 */
	@Test
	public void testUpdateWeather() throws BaseException {
		when(weatherService.updateWeather(weatherRequest, "123")).thenReturn(appResponse);
		assertEquals(appResponse, controller.updateWeather(weatherRequest, "123").getBody());
	}

	/**
	 * Test delete weather.
	 *
	 * @throws BaseException the base exception
	 */
	@Test
	public void testDeleteWeather() throws BaseException {
		when(weatherService.deleteWeather("123", deleteRequest)).thenReturn(appResponse);
		assertEquals(appResponse, controller.deleteWeather("123", deleteRequest).getBody());
	}

	/**
	 * Test delete bulk weather.
	 *
	 * @throws BaseException the base exception
	 */
	@Test
	public void testDeleteBulkWeather() throws BaseException {
		when(weatherService.deleteBulkWeather(deleteRequest)).thenReturn(appResponse);
		assertEquals(appResponse, controller.deleteBulkWeather(deleteRequest).getBody());
	}

	/**
	 * Test search customers by city.
	 *
	 * @throws BaseException the base exception
	 */
	@Test
	public void testSearchCustomersByCity() throws BaseException {
		when(weatherService.searchCustomersByCity("testcity")).thenReturn(listCustomerResponse);
		assertEquals(listCustomerResponse, controller.searchCustomersByCity("testcity").getBody());
	}

	/**
	 * Test search cities by customer.
	 *
	 * @throws BaseException the base exception
	 */
	@Test
	public void testSearchCitiesByCustomer() throws BaseException {
		when(weatherService.searchCitiesByCustomer("456")).thenReturn(listWeatherResponse);
		assertEquals(listWeatherResponse, controller.searchCitiesByCustomer("456").getBody());
	}

	/**
	 * Test search weather by city.
	 *
	 * @throws BaseException the base exception
	 */
	@Test
	public void testSearchWeatherByCity() throws BaseException {
		when(weatherService.searchWeatherByCity(searchRequest, "789")).thenReturn(weatherResponse);
		assertEquals(weatherResponse, controller.searchWeatherByCity(searchRequest, "789").getBody());
	}
}

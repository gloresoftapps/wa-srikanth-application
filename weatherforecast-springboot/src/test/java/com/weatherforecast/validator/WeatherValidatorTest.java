package com.weatherforecast.validator;

import java.math.BigDecimal;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import com.weatherforecast.dto.request.WeatherRequest;
import com.weatherforecast.exception.BaseException;

/**
 * The Class WeatherValidatorTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class WeatherValidatorTest {

	/** The validator. */
	@InjectMocks
	private WeatherValidator validator;

	/** The weather request. */
	private WeatherRequest weatherRequest;
	
	/**
	 * Sets the up.
	 */
	@Before
	public void setUp() {
		weatherRequest = new WeatherRequest();
		weatherRequest.setCity("testcity");
		weatherRequest.setCustomerId("123");
		weatherRequest.setDescription("testdesc");
		weatherRequest.setMaxTemperature(new BigDecimal(12.34));
		weatherRequest.setMinTemperature(new BigDecimal(11.23));
		weatherRequest.setSunRise("2020-11-09T09:36:26");
		weatherRequest.setSunSet("2020-11-09T18:36:26");
		weatherRequest.setTimezone(9600);
		weatherRequest.setTemperature(new BigDecimal(12.54));
	}

	/**
	 * Test validate update weather.
	 *
	 * @throws BaseException the base exception
	 */
	@Test
	public void testValidateUpdateWeather() throws BaseException {
		validator.validateUpdateWeather(weatherRequest);
	}

	/**
	 * Test validate update weather temperature exception.
	 *
	 * @throws BaseException the base exception
	 */
	@Test(expected = BaseException.class)
	public void testValidateUpdateWeatherTemperatureException() throws BaseException {
		weatherRequest = new WeatherRequest();
		weatherRequest.setTemperature(new BigDecimal(90));
		validator.validateUpdateWeather(weatherRequest);
	}

	/**
	 * Test validate update weather date exception.
	 *
	 * @throws BaseException the base exception
	 */
	@Test(expected = BaseException.class)
	public void testValidateUpdateWeatherDateException() throws BaseException {
		weatherRequest = new WeatherRequest();
		weatherRequest.setSunRise("121-06-2020");
		weatherRequest.setTemperature(new BigDecimal(14.56));
		weatherRequest.setSunSet("12-12-2020");
		validator.validateUpdateWeather(weatherRequest);
	}
}

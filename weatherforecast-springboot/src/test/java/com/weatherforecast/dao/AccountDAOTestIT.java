package com.weatherforecast.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.time.LocalDate;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.weatherforecast.config.HibernateConfig;
import com.weatherforecast.entity.Customer;
import com.weatherforecast.exception.BaseException;

/**
 * The Class AccountDAOTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes= {HibernateConfig.class,AccountDAO.class,Customer.class})
@Transactional
@Rollback(true)
public class AccountDAOTestIT {
	
	/** The account DAO. */
	@Autowired
	private AccountDAO accountDAO;

	/**
	 * Test create customer entity.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testCreateCustomerEntity() throws Exception {
		Customer customer = new Customer();
		customer.setCustomerType("basictest");
		customer.setDateOfBirth(LocalDate.now());
		customer.setPassword("test");
		customer.setUserName("testuser");
		assertNotNull(accountDAO.createCustomerEntity(customer));
	}
	
	/**
	 * Test create customer entity base exception.
	 *
	 * @throws BaseException the base exception
	 */
	@Test(expected=BaseException.class)
	public void testCreateCustomerEntityBaseException() throws BaseException {
		Customer customer = new Customer();
		customer.setCustomerType("basictest");
		customer.setDateOfBirth(LocalDate.now());
		customer.setPassword("test");
		customer.setUserName("");
		accountDAO.createCustomerEntity(customer);
	}

	/**
	 * Test get customer login.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testGetCustomerLogin() throws Exception {
		assertNotNull(accountDAO.getCustomerLogin("testuser", "test"));
	}
	
	/**
	 * Test get customer login no result.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testGetCustomerLoginNoResult() throws Exception {
		assertNull(accountDAO.getCustomerLogin("testuser", "testwd"));
	}
}

package com.weatherforecast.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;

import java.time.LocalDate;
import java.time.LocalDateTime;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.client.RestTemplate;

/**
 * The Class WeatherUtilTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class WeatherUtilTest {

	/** The weather util. */
	@InjectMocks
	private WeatherUtil weatherUtil;

	/** The rest template. */
	@Mock
	private RestTemplate restTemplate;

	/**
	 * Test parse date.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testParseDate() throws Exception {
		assertEquals(LocalDate.of(2020, 1, 1), weatherUtil.parseDate("2020-01-01"));
	}

	/**
	 * Test convert long time zone to date.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testConvertLongTimeZoneToDate() throws Exception {
		assertNotNull(weatherUtil.convertLongTimeZoneToDate(1604988799L, 3600));
	}

	/**
	 * Test format local date time to string.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testFormatLocalDateTimeToString() throws Exception {
		assertNotNull(weatherUtil.formatLocalDateTimeToString(LocalDateTime.now()));
	}

	/**
	 * Test convert date str to date.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testConvertDateStrToDate() throws Exception {
		assertEquals(LocalDateTime.of(2020, 1, 1, 5, 0), weatherUtil.convertDateStrToDate("2020-01-01T05:00:00"));
	}

	/**
	 * Test rest exchange.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testRestExchange() throws Exception {
		assertNull(weatherUtil.restExchange(anyString(), any(), any(), any()));
	}

	/**
	 * Test format message.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testFormatMessage() throws Exception {
		assertNotNull("testurlparam", weatherUtil.formatMessage("testurl", "testparam"));
	}
}

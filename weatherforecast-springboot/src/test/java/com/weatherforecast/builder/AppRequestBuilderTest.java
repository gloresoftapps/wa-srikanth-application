package com.weatherforecast.builder;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.weatherforecast.dto.request.AccountRequest;
import com.weatherforecast.dto.request.DeleteRequest;
import com.weatherforecast.dto.request.WeatherRequest;
import com.weatherforecast.entity.Customer;
import com.weatherforecast.entity.Weather;
import com.weatherforecast.util.WeatherUtil;

/**
 * The Class AppRequestBuilderTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class AppRequestBuilderTest {

	/** The request builder. */
	@InjectMocks
	private AppRequestBuilder requestBuilder;

	/** The account request. */
	private AccountRequest accountRequest;

	/** The customer. */
	@Mock
	private Customer customer;

	/** The util. */
	@Mock
	private WeatherUtil util;

	/** The list weather request. */
	private List<WeatherRequest> listWeatherRequest;

	/** The weather request. */
	private WeatherRequest weatherRequest;

	/** The weather. */
	@Mock
	private Weather weather;

	/** The delete request. */
	private DeleteRequest deleteRequest;

	/**
	 * Sets the up.
	 */
	@Before
	public void setUp() {
		weatherRequest = new WeatherRequest();
		weatherRequest.setCity("testcity");
		weatherRequest.setCustomerId("123");
		weatherRequest.setDescription("testdesc");
		weatherRequest.setMaxTemperature(new BigDecimal(12.34));
		weatherRequest.setMinTemperature(new BigDecimal(11.23));
		weatherRequest.setSunRise("9872");
		weatherRequest.setSunSet("7654");
		weatherRequest.setTimezone(9600);
		weatherRequest.setTemperature(new BigDecimal(12.54));
		weather = new Weather();
		deleteRequest = new DeleteRequest();
	}

	/**
	 * Test construct customer.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testConstructCustomer() throws Exception {
		accountRequest = new AccountRequest();
		accountRequest.setUsername("testuser");
		accountRequest.setPassword("testpwd");
		accountRequest.setDateOfBirth("2020-05-01");
		accountRequest.setType("basictest");
		assertNotNull(requestBuilder.constructCustomer(accountRequest));
	}

	/**
	 * Test construct weather.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testConstructWeather() throws Exception {
		listWeatherRequest = new ArrayList<>();
		listWeatherRequest.add(weatherRequest);
		assertNotNull(requestBuilder.constructWeather(listWeatherRequest));
	}

	/**
	 * Test get create weather object.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testGetCreateWeatherObject() throws Exception {
		assertNotNull(requestBuilder.getCreateWeatherObject(weatherRequest, weather));
	}

	/**
	 * Test get update weather object.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testGetUpdateWeatherObject() throws Exception {
		assertEquals(weather, requestBuilder.getUpdateWeatherObject(weatherRequest, weather));
	}

	/**
	 * Test construct bulk delete weather.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testConstructBulkDeleteWeather() throws Exception {
		List<String> listWeatherId = new ArrayList<>();
		listWeatherId.add("123");
		deleteRequest.setWeathers(listWeatherId);
		assertNotNull(requestBuilder.constructBulkDeleteWeather(deleteRequest));
	}

	/**
	 * Test get MD 5 password.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testGetMD5Password() throws Exception {
		assertNotNull(requestBuilder.getMD5Password("testpwd"));
	}
}

package com.weatherforecast.builder;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.IOException;
import java.io.InputStream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.weatherforecast.util.WeatherUtil;

/**
 * The Class JSONResponseBuilderTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class JSONResponseBuilderTest {
	
	/** The response builder. */
	@InjectMocks
	private JSONResponseBuilder responseBuilder;
	
	@Mock
	private WeatherUtil weatherUtil;

	/**
	 * Test construct weather object.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testConstructWeatherObject() throws Exception {
		assertNotNull(responseBuilder.constructWeatherObject(fetchWeatherDetails()));
	}
	
	/**
	 * Fetch weather details.
	 *
	 * @return the string
	 * @throws JsonParseException the json parse exception
	 * @throws JsonMappingException the json mapping exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static String fetchWeatherDetails() throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		InputStream is = JSONResponseBuilderTest.class.getResourceAsStream("/test_weather.json");
		Object obj = mapper.readValue(is, Object.class);
		return mapper.writeValueAsString(obj);
	}

}

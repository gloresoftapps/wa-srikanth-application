package com.weatherforecast.builder;

import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.weatherforecast.entity.Customer;
import com.weatherforecast.entity.Weather;
import com.weatherforecast.util.WeatherUtil;

/**
 * The Class AppResponseBuilderTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class AppResponseBuilderTest {
	
	/** The response builder. */
	@InjectMocks
	private AppResponseBuilder responseBuilder;
	
	/** The list weather. */
	private List<Weather> listWeather;
	
	/** The weather. */
	private Weather weather;
	
	/** The customer. */
	private Customer customer;
	
	/** The customers. */
	private List<Customer> customers;
	
	/** The util. */
	@Mock
	private WeatherUtil util;
	
	/**
	 * Sets the up.
	 */
	@Before
	public void setUp() {
		weather = new Weather();
		weather.setCity("testcity");
		weather.setId("123");
		weather.setDescription("testdesc");
		weather.setMaxTemperature(new BigDecimal(12.34));
		weather.setMinTemperature(new BigDecimal(11.23));
		weather.setSunRise(LocalDateTime.now());
		weather.setSunSet(LocalDateTime.now());		
		weather.setTemperature(new BigDecimal(12.54));
		listWeather = new ArrayList<>();
		listWeather.add(weather);
		customer = new Customer();
		customers = new ArrayList<>();
		customer.setCustomerType("testtype");
		customer.setId("123");
		customer.setPassword("testpwd");
		customer.setUserName("testuser");
		customers.add(customer);		
	}


	/**
	 * Test construct response.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testConstructResponse() throws Exception {
		assertNotNull(responseBuilder.constructResponse("testmsg", true));
	}

	/**
	 * Test construct weather history response.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testConstructWeatherHistoryResponse() throws Exception {
		assertNotNull(responseBuilder.constructWeatherHistoryResponse(listWeather));
	}

	/**
	 * Test construct customers.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testConstructCustomers() throws Exception {
		assertNotNull(responseBuilder.constructCustomers(customers));
	}

	/**
	 * Test construct customer.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testConstructCustomer() throws Exception {
		assertNotNull(responseBuilder.constructCustomer(customer));
	}
	
	@Test
	public void testGetWeatherResponse() {
		assertNotNull(responseBuilder.getWeatherResponse(weather));
	}
}

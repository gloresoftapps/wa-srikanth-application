package com.weatherforecast.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.weatherforecast.builder.AppRequestBuilder;
import com.weatherforecast.builder.AppResponseBuilder;
import com.weatherforecast.builder.JSONResponseBuilder;
import com.weatherforecast.builder.JSONResponseBuilderTest;
import com.weatherforecast.config.WeatherConfig;
import com.weatherforecast.dao.WeatherDAO;
import com.weatherforecast.dto.request.DeleteRequest;
import com.weatherforecast.dto.request.SearchRequest;
import com.weatherforecast.dto.request.WeatherRequest;
import com.weatherforecast.dto.response.AppResponse;
import com.weatherforecast.dto.response.CustomerResponse;
import com.weatherforecast.dto.response.WeatherResponse;
import com.weatherforecast.entity.Weather;
import com.weatherforecast.util.WeatherUtil;

/**
 * The Class WeatherServiceImplTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class WeatherServiceImplTest {
	
	/** The weather service. */
	@InjectMocks
	private WeatherServiceImpl weatherService;
	
	/** The list weather request. */
	@Mock
	private List<WeatherRequest> listWeatherRequest;
	
	/** The weather DAO. */
	@Mock
	private WeatherDAO weatherDAO;
	
	/** The request builder. */
	@Mock
	private AppRequestBuilder requestBuilder;
	
	/** The response builder. */
	@Mock
	private AppResponseBuilder responseBuilder;
	
	/** The list weather. */
	@Mock
	private List<Weather> listWeather;
	
	/** The app response. */
	@Mock
	private AppResponse appResponse;
	
	/** The list weather response. */
	@Mock
	private List<WeatherResponse> listWeatherResponse;
	
	/** The weather request. */
	@Mock
	private WeatherRequest weatherRequest;
	
	/** The weather. */
	@Mock
	private Weather weather;
	
	/** The delete request. */
	@Mock
	private DeleteRequest deleteRequest;
	
	/** The list customer response. */
	@Mock
	private List<CustomerResponse> listCustomerResponse;
	
	/** The search request. */
	private SearchRequest searchRequest;
	
	/** The util. */
	@Mock
	private WeatherUtil util;
	
	/** The config. */
	@Mock
	private WeatherConfig config;
	
	@Mock
	private RestTemplate restTemplate;
	
	@Mock
	private JSONResponseBuilder jsonBuilder;
	
	@Mock
	private WeatherResponse weatherResponse;

	/**
	 * Test read weather history.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testReadWeatherHistory() throws Exception {
		assertTrue(weatherService.readWeatherHistory("123").isEmpty());		
	}

	/**
	 * Test update weather.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testUpdateWeather() throws Exception {
		when(weatherDAO.getWeatherById("456")).thenReturn(weather);
		assertNull(weatherService.updateWeather(weatherRequest, "456"));
		
	}

	/**
	 * Test delete weather.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testDeleteWeather() throws Exception {
		assertNull(weatherService.deleteWeather("789", deleteRequest));
	}

	/**
	 * Test delete bulk weather.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testDeleteBulkWeather() throws Exception {
		assertNull(weatherService.deleteBulkWeather(deleteRequest));
	}

	/**
	 * Test search customers by city.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testSearchCustomersByCity() throws Exception {
		assertEquals(new ArrayList<>(),weatherService.searchCustomersByCity("testcity"));
	}

	/**
	 * Test search cities by customer.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testSearchCitiesByCustomer() throws Exception {
		assertEquals(new ArrayList<>(), weatherService.searchCitiesByCustomer("123"));
	}

	@Test
	public void testSearchWeatherByCity() throws Exception {
		searchRequest = new SearchRequest();
		searchRequest.setCity("testcity");
		searchRequest.setName("testname");
		when(util.formatMessage(any(), any())).thenReturn("testurl");
		ResponseEntity<String> response = new ResponseEntity<>(JSONResponseBuilderTest.fetchWeatherDetails(),HttpStatus.OK);
		when(util.restExchange(anyString(), any(), any(), any())).thenReturn(response);
		when(jsonBuilder.constructWeatherObject(response.getBody())).thenReturn(weatherResponse);		
		assertNotNull(weatherService.searchWeatherByCity(searchRequest, "123"));
	}

	/*
	 * @Test public void testSaveWeatherHistory() throws Exception { String
	 * weatherJson = JSONResponseBuilderTest.fetchWeatherDetails(); jsonBuilder =
	 * new JSONResponseBuilder();
	 * 
	 * WeatherResponse weatherObj = jsonBuilder.constructWeatherObject(weatherJson);
	 * weatherService.saveWeatherHistory("testuser", weatherObj, "123");
	 * assertNotNull(weatherObj); }
	 */
	
}

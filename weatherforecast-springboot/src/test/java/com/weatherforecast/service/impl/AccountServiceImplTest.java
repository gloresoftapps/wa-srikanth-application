package com.weatherforecast.service.impl;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;

import com.weatherforecast.builder.AppRequestBuilder;
import com.weatherforecast.builder.AppResponseBuilder;
import com.weatherforecast.dao.AccountDAO;
import com.weatherforecast.dto.request.AccountRequest;
import com.weatherforecast.dto.request.CustomerRequest;

/**
 * The Class AccountServiceImplTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class AccountServiceImplTest {

	/** The service. */
	@InjectMocks
	private AccountServiceImpl service;

	/** The acct request. */
	private AccountRequest acctRequest;

	/** The customer request. */
	private CustomerRequest customerRequest;

	/** The account DAO. */
	@Mock
	private AccountDAO accountDAO;

	/** The response builder. */
	@Mock
	private AppResponseBuilder responseBuilder;

	/** The request builder. */
	@Mock
	private AppRequestBuilder requestBuilder;

	/**
	 * Test register account.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testRegisterAccount() throws Exception {
		acctRequest = new AccountRequest();
		acctRequest.setUsername("testuser");
		acctRequest.setDateOfBirth("2020-06-05");
		acctRequest.setType("basic");
		acctRequest.setPassword("pwdtest");
		assertNull(service.registerAccount(acctRequest));
	}

	/**
	 * Test get login info.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testGetLoginInfo() throws Exception {
		customerRequest = new CustomerRequest();
		customerRequest.setUsername("testuser");
		customerRequest.setPassword("testpwd");
		Map<String,Object> response = service.getLoginInfo(customerRequest);
		HttpStatus httpStatus = (HttpStatus) response.getOrDefault("httpStatus", HttpStatus.OK);
		assertEquals(HttpStatus.UNAUTHORIZED, httpStatus);
	}
}

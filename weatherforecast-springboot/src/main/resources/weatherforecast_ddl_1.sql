CREATE TABLE customer (
	id VARCHAR PRIMARY KEY,
	username VARCHAR (50) UNIQUE,
	password VARCHAR (50),
	dateofbirth VARCHAR (50) UNIQUE,
	customertype VARCHAR (50)
);

CREATE TABLE weather (
	id VARCHAR PRIMARY KEY,
	city VARCHAR (50) UNIQUE,
	description VARCHAR (50),
	temperature float (50),
	mintemperature float (50),
	maxtemperature float (50),
	sunrise timestamp,
	sunset timestamp,
	customerId VARCHAR (50) REFERENCES customer (id)
);

CREATE TABLE history (
	id VARCHAR PRIMARY KEY,
	customerId VARCHAR (50) REFERENCES customer (id),
	weatherId VARCHAR (50) REFERENCES weather (id),
	createddate timestamp 
);

package com.weatherforecast.dao;

import javax.persistence.NoResultException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.weatherforecast.constant.Constant;
import com.weatherforecast.entity.Customer;
import com.weatherforecast.exception.BaseException;

/**
 * The Class AccountDAO.
 */
@Repository
@Transactional(rollbackFor=Exception.class)
public class AccountDAO {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(AccountDAO.class);

	/** The session factory. */
	@Autowired
	private SessionFactory sessionFactory;

	/**
	 * Creates the customer entity.
	 *
	 * @param customer the customer
	 * @return the string
	 * @throws BaseException 
	 */
	public String createCustomerEntity(Customer customer) throws BaseException {
		LOGGER.info("Entering the method createCustomerEntity in dao");
		Session session = sessionFactory.openSession();
		String customerId = null;
		try {
			Transaction transaction = session.beginTransaction();
			customerId = (String) session.save(customer);
			transaction.commit();
		}
		catch(Exception e) {		
			throw new BaseException(Constant.REGISTRATION_FAILED_MSG,
					Constant.REGISTRATION_ERROR_CODE, e);
		}
		finally {			
				session.close();			
		}						
		LOGGER.info("Entering the method createCustomerEntity in dao");
		return customerId;
	}

	/**
	 * Gets the customer login.
	 *
	 * @param username the username
	 * @param password the password
	 * @return the customer login
	 * @throws BaseException 
	 */
	public Customer getCustomerLogin(String username, String password)  {
		LOGGER.info("Entering the method getCustomerLogin in dao");
		Session session = sessionFactory.openSession();
		Customer customer = null;
		try {
			Transaction transaction = session.beginTransaction();
			String hql = "from Customer s where s.userName = :username and s.password = :pwd";
			Query<Customer> query = session.createQuery(hql, Customer.class);
			query.setParameter("username", username);
			query.setParameter("pwd", password);
			customer = query.getSingleResult();
			transaction.commit();		
		}
		catch(NoResultException e) {
			LOGGER.info("No customer found for given input");
		}
		finally {
			session.close();
		}						
		LOGGER.info("Exiting the method getCustomerLogin in dao");
		return customer;
	}
}

package com.weatherforecast.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.weatherforecast.entity.Customer;
import com.weatherforecast.entity.History;
import com.weatherforecast.entity.Weather;

/**
 * The Class WeatherDAO.
 */
@Repository
@Transactional(rollbackFor = Exception.class)
public class WeatherDAO {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(WeatherDAO.class);

	/** The session factory. */
	@Autowired
	private SessionFactory sessionFactory;

	/**
	 * Creates the weather entity.
	 *
	 * @param listWeather the list weather
	 */
	public void createWeatherEntity(List<Weather> listWeather) {
		LOGGER.info("Entering the method createWeatherEntity in dao");
		Session session = null;
		try {
			session = sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();
			for (Weather weather : listWeather) {
				session.saveOrUpdate(weather);
			}
			transaction.commit();
		} finally {
			if (null != session) {
				session.close();
			}
		}
		LOGGER.info("Exiting the method createWeatherEntity in dao");
	}

	/**
	 * Read weather history.
	 *
	 * @param userId the user id
	 * @return the list
	 */
	public List<Weather> readWeatherHistory(String userId) {
		LOGGER.info("Entering the method readWeatherHistory in dao");
		Session session = null;
		List<Weather> listWeather = null;
		try {
			session = sessionFactory.openSession();
			session.beginTransaction();
			Query<Weather> query = session.createQuery("select h.weather from History h where h.customer.id=:userId",
					Weather.class);
			query.setParameter("userId", userId);
			listWeather = query.getResultList();
			session.getTransaction().commit();
		} finally {
			if (null != session) {
				session.close();
			}
		}
		LOGGER.info("Exiting the method readWeatherHistory in dao");
		return listWeather;
	}

	/**
	 * Update weather entity.
	 *
	 * @param weather the weather
	 */
	public void updateWeatherEntity(Weather weather) {
		LOGGER.info("Entering the method updateWeatherEntity in dao");
		Session session = null;
		try {
			session = sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();
			session.update(weather);
			transaction.commit();
		} finally {
			if (null != session) {
				session.close();
			}
		}
		LOGGER.info("Exiting the method updateWeatherEntity in dao");
	}

	/**
	 * Delete weather entity.
	 *
	 * @param weather the weather
	 */
	public void deleteWeatherEntity(Weather weather) {
		LOGGER.info("Entering the method deleteWeatherEntity in dao");
		Session session = null;
		try {
			session = sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();
			session.delete(weather);
			transaction.commit();
		} finally {
			if (null != session) {
				session.close();
			}
		}
		LOGGER.info("Exiting the method deleteWeatherEntity in dao");
	}

	/**
	 * Delete bulk weather entity.
	 *
	 * @param listWeather the list weather
	 */
	public void deleteBulkWeatherEntity(List<Weather> listWeather) {
		LOGGER.info("Entering the method deleteBulkWeatherEntity in dao");
		Session session = null;
		try {
			session = sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();
			for (Weather weather : listWeather) {
				session.delete(weather);
			}
			transaction.commit();
		} finally {
			if (null != session) {
				session.close();
			}
		}
		LOGGER.info("Exiting the method deleteBulkWeatherEntity in dao");
	}

	/**
	 * Gets the customers by city.
	 *
	 * @param city the city
	 * @return the customers by city
	 */
	public List<Customer> getCustomersByCity(String city) {
		LOGGER.info("Entering the method getCustomersByCity in dao");
		Session session = null;
		List<Customer> listCustomer = null;
		try {
			session = sessionFactory.openSession();
			session.beginTransaction();
			Query<Customer> query = session.createQuery(
					"select h.customer from History h ,Customer cust,Weather w \r\n"
							+ "where cust.id = h.customer.id and w.id=h.weather.id and w.city=:cityname",
					Customer.class);
			query.setParameter("cityname", city);
			listCustomer = query.getResultList();
			session.getTransaction().commit();
		} finally {
			if (null != session) {
				session.close();
			}
		}
		LOGGER.info("Exiting the method getCustomersByCity in dao");
		return listCustomer;
	}

	/**
	 * Gets the cities by customer.
	 *
	 * @param customerId the customer id
	 * @return the cities by customer
	 */
	public List<Weather> getCitiesByCustomer(String customerId) {
		LOGGER.info("Entering the method getCitiesByCustomer in dao");
		Session session = null;
		List<Weather> listWeather = null;
		try {
			session = sessionFactory.openSession();
			session.beginTransaction();
			Query<Weather> query = session.createQuery(
					"select h.weather from History h, Customer cust, Weather w where h.customer.id = cust.id\r\n"
							+ "and h.weather.id=w.id and cust.userName=:customerId",
					Weather.class);
			query.setParameter("customerId", customerId);
			listWeather = query.getResultList();
			session.getTransaction().commit();
		} finally {
			if (null != session) {
				session.close();
			}
		}
		LOGGER.info("Exiting the method getCitiesByCustomer in dao");
		return listWeather;
	}

	/**
	 * Creates the history entity.
	 *
	 * @param history the history
	 */
	public void createHistoryEntity(History history) {
		LOGGER.info("Entering the method createHistoryEntity in dao");
		Session session = null;
		try {
			session = sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();
			session.saveOrUpdate(history);
			transaction.commit();
		} finally {
			if (null != session) {
				session.close();
			}
		}
		LOGGER.info("Exiting the method createHistoryEntity in dao");
	}

	/**
	 * Search weather by city.
	 *
	 * @param city the city
	 * @return the weather
	 */
	public Weather searchWeatherByCity(String city) {
		LOGGER.info("Entering the method searchWeatherByCity in dao");
		Session session = null;
		Weather weather = null;
		try {
			session = sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();
			Query<Weather> query = session.createQuery("select w from Weather w where " + "w.city=:cityname",
					Weather.class);
			query.setParameter("cityname", city);
			weather = query.getSingleResult();
			transaction.commit();
		} finally {
			if (null != session) {
				session.close();
			}
		}
		LOGGER.info("Exiting the method searchWeatherByCity in dao");
		return weather;
	}

	/**
	 * Gets the weather by id.
	 *
	 * @param weatherId the weather id
	 * @return the weather by id
	 */
	public Weather getWeatherById(String weatherId) {
		LOGGER.info("Entering the method getWeatherById in dao");
		Session session = null;
		Weather weather = null;
		try {
			session = sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();
			weather = session.find(Weather.class, weatherId);
			transaction.commit();
		} finally {
			if (null != session) {
				session.close();
			}
		}
		LOGGER.info("Exiting the method getWeatherById in dao");
		return weather;
	}

	/**
	 * Gets the history by weather customer id.
	 *
	 * @param weatherId the weather id
	 * @param customerId the customer id
	 * @return the history by weather customer id
	 */
	public List<History> getHistoryByWeatherCustomerId(String weatherId, String customerId) {
		LOGGER.info("Entering the method getHistoryByCustomerId in dao");
		Session session = null;
		List<History> listHistory = null;
		try {
			session = sessionFactory.openSession();
			session.beginTransaction();
			NativeQuery<History> nativeQuery = session.createNativeQuery(
					"select id,weatherId,customerId,createddate from History h where h.customerid=? and h.weatherid=?",
					History.class);
			nativeQuery.setParameter(1, customerId);
			nativeQuery.setParameter(2, weatherId);
			listHistory = nativeQuery.getResultList();
			session.getTransaction().commit();
		} finally {
			if (null != session) {
				session.close();
			}
		}
		LOGGER.info("Exiting the method getHistoryByCustomerId in dao");
		return listHistory;
	}

	/**
	 * Gets the bulk history.
	 *
	 * @param customerId the customer id
	 * @param listWeatherId the list weather id
	 * @return the bulk history
	 */
	public List<History> getBulkHistory(String customerId, List<String> listWeatherId) {
		LOGGER.info("Entering the method getBulkHistory in dao");
		Session session = null;
		List<History> listHistory = null;
		try {
			session = sessionFactory.openSession();
			session.beginTransaction();
			NativeQuery<History> nativeQuery = session.createNativeQuery(
					"select id,weatherId,customerId,createddate from History h where h.customerid=:customerId and h.weatherid in (:listWeatherId)",
					History.class);
			nativeQuery.setParameter("customerId", customerId);
			nativeQuery.setParameter("listWeatherId", listWeatherId);
			listHistory = nativeQuery.getResultList();
			session.getTransaction().commit();
		} finally {
			if (null != session) {
				session.close();
			}
		}
		LOGGER.info("Exiting the method getBulkHistory in dao");
		return listHistory;
	}

	/**
	 * Delete history entity.
	 *
	 * @param listHistory the list history
	 */
	public void deleteHistoryEntity(List<History> listHistory) {
		LOGGER.info("Entering the method deleteBulkWeatherEntity in dao");
		Session session = null;
		try {
			session = sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();
			for (History history : listHistory) {
				session.delete(history);
			}
			transaction.commit();
		} finally {
			if (null != session) {
				session.close();
			}
		}
		LOGGER.info("Exiting the method deleteBulkWeatherEntity in dao");
	}
}

package com.weatherforecast.builder;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.weatherforecast.constant.Constant;
import com.weatherforecast.dto.response.WeatherResponse;
import com.weatherforecast.util.WeatherUtil;

/**
 * The Class JSONResponseBuilder.
 */
@Component
public class JSONResponseBuilder {
	
	/** The util. */
	@Autowired
	private WeatherUtil util;

	/**
	 * Construct weather object.
	 *
	 * @param inputJson the input json
	 * @return the weather response
	 */
	public WeatherResponse constructWeatherObject(String inputJson) {
		
		JSONObject requestObj = new JSONObject(inputJson);
		
		JSONArray requestArray = requestObj.getJSONArray("list");
		JSONObject weatherData = requestArray.getJSONObject(0);
		JSONObject temperatureObj = weatherData.getJSONObject("main");
		double temperature = temperatureObj.getDouble(Constant.TEMP);
		double minTemperature = temperatureObj.getDouble(Constant.TEMP_MIN);					
		double maxTemperature = temperatureObj.getDouble(Constant.TEMP_MAX);
		
		JSONArray weatherDescriptionArr = weatherData.getJSONArray(Constant.WEATHER);
		JSONObject weatherDescription = weatherDescriptionArr.getJSONObject(0);
		String description = weatherDescription.getString(Constant.DESCRIPTION);
		
		JSONObject cityObj = requestObj.getJSONObject(Constant.CITY);
		String cityName = cityObj.getString(Constant.NAME);
		int timezone = cityObj.getInt(Constant.TIMEZONE);
		long sunRise = cityObj.getLong(Constant.SUNRISE);
		long sunSet = cityObj.getLong(Constant.SUNSET);
				
		WeatherResponse weatherResponse = new WeatherResponse();
		
		weatherResponse.setCity(cityName);
		weatherResponse.setDescription(description);
		
		String tempStr = String.valueOf(temperature);
		BigDecimal temp = new BigDecimal(tempStr);
		weatherResponse.setTemperature(temp);
		
		String minTempStr = String.valueOf(minTemperature);
		BigDecimal tempMin = new BigDecimal(minTempStr);
		weatherResponse.setMinTemperature(tempMin);
		
		String maxTempStr = String.valueOf(maxTemperature);
		BigDecimal tempMax = new BigDecimal(maxTempStr);
		weatherResponse.setMaxTemperature(tempMax);
		
		LocalDateTime sunRiseDate = util.convertLongTimeZoneToDate(sunRise, timezone);
		String sunRiseStr = util.formatLocalDateTimeToString(sunRiseDate);
		weatherResponse.setSunRise(sunRiseStr);
		LocalDateTime sunSetDate = util.convertLongTimeZoneToDate(sunSet, timezone);
		String sunSetStr = util.formatLocalDateTimeToString(sunSetDate);
		weatherResponse.setSunSet(sunSetStr);
		
		return weatherResponse;
	}
}

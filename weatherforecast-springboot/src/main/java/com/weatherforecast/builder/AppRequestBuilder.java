package com.weatherforecast.builder;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.weatherforecast.constant.Constant;
import com.weatherforecast.dto.request.AccountRequest;
import com.weatherforecast.dto.request.DeleteRequest;
import com.weatherforecast.dto.request.WeatherRequest;
import com.weatherforecast.entity.Customer;
import com.weatherforecast.entity.Weather;
import com.weatherforecast.exception.BaseException;
import com.weatherforecast.util.WeatherUtil;

/**
 * The Class AppRequestBuilder.
 */
@Component
public class AppRequestBuilder {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(AppRequestBuilder.class);

	/** The util. */
	@Autowired
	private WeatherUtil util;

	/**
	 * Construct customer.
	 *
	 * @param request the request
	 * @return the customer
	 * @throws BaseException
	 */
	public Customer constructCustomer(AccountRequest request) throws BaseException {
		LOGGER.info("Entering the method constructCustomer in request builder");
		Customer customer = new Customer();
		customer.setUserName(request.getUsername());
		customer.setPassword(getMD5Password(request.getPassword()));
		LocalDate dateOfBirth = util.parseDate(request.getDateOfBirth());
		customer.setDateOfBirth(dateOfBirth);
		customer.setCustomerType(request.getType());
		LOGGER.info("Exiting the method constructCustomer in request builder");
		return customer;
	}

	/**
	 * Construct weather.
	 *
	 * @param listRequest the list request
	 * @return the list
	 */
	public List<Weather> constructWeather(List<WeatherRequest> listRequest) {
		LOGGER.info("Entering the method constructWeather in request builder");
		List<Weather> listWeather = listRequest.stream().map(request -> getCreateWeatherObject(request, new Weather()))
				.collect(Collectors.toList());
		LOGGER.info("Exiting the method constructWeather in request builder");
		return listWeather;

	}

	/**
	 * Gets the creates the weather object.
	 *
	 * @param request the request
	 * @param weather the weather
	 * @return the creates the weather object
	 */
	public Weather getCreateWeatherObject(WeatherRequest request, Weather weather) {
		LOGGER.info("Entering the method getCreateWeatherObject in request builder");
		LocalDateTime dateObj;
		weather.setCity(request.getCity().toLowerCase());
		weather.setDescription(request.getDescription());
		weather.setTemperature(request.getTemperature());
		weather.setMinTemperature(request.getMinTemperature());
		weather.setMaxTemperature(request.getMaxTemperature());
		String sunRise = request.getSunRise();
		Long sunRiseLong = Long.parseLong(sunRise);
		String sunSet = request.getSunSet();
		Long sunSetLong = Long.parseLong(sunSet);
		dateObj = util.convertLongTimeZoneToDate(sunRiseLong, request.getTimezone());
		weather.setSunRise(dateObj);
		dateObj = util.convertLongTimeZoneToDate(sunSetLong, request.getTimezone());
		weather.setSunSet(dateObj);
		LOGGER.info("Exiting the method getCreateWeatherObject in request builder");
		return weather;
	}

	/**
	 * Gets the update weather object.
	 *
	 * @param request the request
	 * @param weather the weather
	 * @return the update weather object
	 */
	public Weather getUpdateWeatherObject(WeatherRequest request, Weather weather) {
		LOGGER.info("Entering the method getUpdateWeatherObject in request builder");
		LocalDateTime dateObj;
		weather.setDescription(request.getDescription());
		weather.setTemperature(request.getTemperature());
		dateObj = util.convertDateStrToDate(request.getSunRise());
		weather.setSunRise(dateObj);
		dateObj = util.convertDateStrToDate(request.getSunSet());
		weather.setSunSet(dateObj);
		LOGGER.info("Exiting the method getUpdateWeatherObject in request builder");
		return weather;
	}

	/**
	 * Construct bulk delete weather.
	 *
	 * @param request the request
	 * @return the list
	 */
	public List<Weather> constructBulkDeleteWeather(DeleteRequest request) {
		LOGGER.info("Entering the method constructBulkDeleteWeather in request builder");
		List<Weather> listWeather = new ArrayList<>();
		request.getWeathers().forEach(id -> {
			Weather weather = new Weather();
			weather.setId(id);
			listWeather.add(weather);
		});
		LOGGER.info("Exiting the method constructBulkDeleteWeather in request builder");
		return listWeather;
	}

	/**
	 * Gets the MD 5 password.
	 *
	 * @param inputPassword the input password
	 * @return the MD 5 password
	 * @throws BaseException
	 */
	public String getMD5Password(String inputPassword) throws BaseException {
		LOGGER.info("Entering the method getMD5Password in request builder");
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] messageDigest = md.digest(inputPassword.getBytes());
			BigInteger no = new BigInteger(1, messageDigest);

			String hashtext = no.toString(16);
			while (hashtext.length() < 32) {
				hashtext = "0" + hashtext;
			}
			LOGGER.info("Exiting the method getMD5Password in request builder");
			return hashtext;
		} catch (NoSuchAlgorithmException e) {
			throw new BaseException(Constant.MD5_ERR_CODE, "Login API Failed.Please check logs",
					HttpStatus.INTERNAL_SERVER_ERROR, e);
		}
	}
}

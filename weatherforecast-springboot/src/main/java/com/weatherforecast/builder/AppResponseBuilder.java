package com.weatherforecast.builder;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.weatherforecast.dto.response.AppResponse;
import com.weatherforecast.dto.response.CustomerResponse;
import com.weatherforecast.dto.response.WeatherResponse;
import com.weatherforecast.entity.Customer;
import com.weatherforecast.entity.Weather;
import com.weatherforecast.util.WeatherUtil;

/**
 * The Class AppResponseBuilder.
 */
@Component
public class AppResponseBuilder {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(AppResponseBuilder.class);

	/** The util. */
	@Autowired
	private WeatherUtil util;

	/**
	 * Construct response.
	 *
	 * @param message the message
	 * @param success the success
	 * @return the app response
	 */
	public AppResponse constructResponse(String message, boolean success) {
		AppResponse response = new AppResponse();
		response.setMessage(message);
		response.setSuccess(success);
		return response;
	}

	/**
	 * Construct weather history response.
	 *
	 * @param listWeather the list weather
	 * @return the list
	 */
	public List<WeatherResponse> constructWeatherHistoryResponse(List<Weather> listWeather) {
		LOGGER.info("Entering the method constructWeatherHistoryResponse in response builder");

		listWeather = listWeather.stream().filter(getDistinctObjectByKey(Weather::getCity))
				.collect(Collectors.toList());

		List<WeatherResponse> listWeatherResponse = listWeather.stream().map(this::getWeatherResponse)
				.collect(Collectors.toList());
		LOGGER.info("Exiting the method constructWeatherHistoryResponse in response builder");
		return listWeatherResponse;
	}

	/**
	 * Gets the weather response.
	 *
	 * @param weather the weather
	 * @return the weather response
	 */
	public WeatherResponse getWeatherResponse(Weather weather) {
		WeatherResponse weatherResponse = new WeatherResponse();
		weatherResponse.setWeatherId(weather.getId());
		weatherResponse.setCity(weather.getCity());
		weatherResponse.setDescription(weather.getDescription());
		weatherResponse.setTemperature(weather.getTemperature());
		weatherResponse.setMinTemperature(weather.getMinTemperature());
		weatherResponse.setMaxTemperature(weather.getMaxTemperature());
		weatherResponse.setSunRise(util.formatLocalDateTimeToString(weather.getSunRise()));
		weatherResponse.setSunSet(util.formatLocalDateTimeToString(weather.getSunSet()));
		return weatherResponse;
	}

	/**
	 * Construct customers.
	 *
	 * @param customers the customers
	 * @return the list
	 */
	public List<CustomerResponse> constructCustomers(List<Customer> customers) {
		LOGGER.info("Entering the method constructCustomers in response builder");

		customers = customers.stream().filter(getDistinctObjectByKey(Customer::getUserName))
				.collect(Collectors.toList());

		List<CustomerResponse> listCustomerResponse = customers.stream().map(this::constructCustomer)
				.collect(Collectors.toList());
		LOGGER.info("Exiting the method constructCustomers in response builder");
		return listCustomerResponse;
	}

	/**
	 * Construct customer.
	 *
	 * @param customer the customer
	 * @return the customer response
	 */
	public CustomerResponse constructCustomer(Customer customer) {
		LOGGER.info("Entering the method constructCustomers in response builder");
		CustomerResponse customerResponse = new CustomerResponse();
		customerResponse.setCustomerId(customer.getId());
		customerResponse.setUsername(customer.getUserName());
		customerResponse.setType(customer.getCustomerType());
		LOGGER.info("Exiting the method constructCustomers in response builder");
		return customerResponse;
	}

	/**
	 * Gets the distinct object by key.
	 *
	 * @param <T> the generic type
	 * @param keyExtractor the key extractor
	 * @return the distinct weather by key
	 */
	public static <T> Predicate<T> getDistinctObjectByKey(Function<? super T, Object> keyExtractor) {
		Map<Object, Boolean> map = new ConcurrentHashMap<>();
		return t -> map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
	}
}

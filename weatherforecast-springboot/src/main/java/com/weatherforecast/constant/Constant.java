package com.weatherforecast.constant;

/**
 * The Class Constant.
 */
public class Constant {
	
	/** The Constant REGISTER_SUCCESS_MSG. */
	public static final String REGISTER_SUCCESS_MSG = "User registered successfully";
	
	/** The Constant LOGIN_SUCCESS_MSG. */
	public static final String LOGIN_SUCCESS_MSG = "Login success";
	
	/** The Constant LOGIN_FAILURE_MSG. */
	public static final String LOGIN_FAILURE_MSG = "Login Failed.Please try again with valid credentials.";
	
	/** The Constant CREATE_WEATHER_SUCCESS_MSG. */
	public static final String CREATE_WEATHER_SUCCESS_MSG = "Weather created successfully";
	
	/** The Constant UPDATE_WEATHER_SUCCESS_MSG. */
	public static final String UPDATE_WEATHER_SUCCESS_MSG = "Weather updated successfully";
	
	/** The Constant DELETE_WEATHER_SUCCESS_MSG. */
	public static final String DELETE_WEATHER_SUCCESS_MSG = "Weather deleted successfully";
	
	public static final String WEATHER_NOT_FOUND = "Weather not found. Please try with valid weatherId";
	
	/** The Constant BULK_DELETE_WEATHER_SUCCESS_MSG. */
	public static final String BULK_DELETE_WEATHER_SUCCESS_MSG = "Bulk weather deleted successfully";
	
	/** The Constant ERROR_CODE. */
	public static final String ERROR_CODE = "errorCode";
	
	/** The Constant STATUS_CODE. */
	public static final String STATUS_CODE = "statusCode";
	
	/** The Constant MESSAGE. */
	public static final String MESSAGE = "message";
	
	/** The Constant LOG_TRACKING_ID. */
	public static final String LOG_TRACKING_ID = "logTrackingId";
	
	/** The Constant ERROR. */
	public static final String ERROR = "error";
	
	/** The Constant REGISTRATION_ERROR_CODE. */
	public static final String REGISTRATION_ERROR_CODE = "1000";
	
	/** The Constant LOGIN_API_ERROR. */
	public static final String LOGIN_API_ERROR = "1001";
	
	/** The Constant CREATEWEATHER_API_ERROR. */
	public static final String CREATEWEATHER_API_ERROR = "1002";
	
	/** The Constant READWEATHERHISTORY_API_ERROR. */
	public static final String READWEATHERHISTORY_API_ERROR = "1003";
	
	/** The Constant UPDATE_WEATHER_ERR_CODE. */
	public static final String UPDATE_WEATHER_ERR_CODE = "1004";
	
	/** The Constant DELETE_WEATHER_ERR_CODE. */
	public static final String DELETE_WEATHER_ERR_CODE = "1005";
	
	/** The Constant DELETEBULK_WEATHER_ERR_CODE. */
	public static final String DELETEBULK_WEATHER_ERR_CODE = "1006";
	
	/** The Constant CUSTOMERS_BYCITY_ERR_CODE. */
	public static final String CUSTOMERS_BYCITY_ERR_CODE = "1007";
	
	/** The Constant CITIES_BYCUSTOMER_ERR_CODE. */
	public static final String CITIES_BYCUSTOMER_ERR_CODE = "1008";
	
	/** The Constant WEATHER_BYCITY_ERR_CODE. */
	public static final String WEATHER_BYCITY_ERR_CODE = "1009";
	
	/** The Constant CITY_ALREADY_EXISTS. */
	public static final String CITY_ALREADY_EXISTS = "1010";
	
	/** The Constant BAD_REQUEST_ERROR. */
	public static final String BAD_REQUEST_ERROR = "4000";
	
	/** The Constant USERNAME. */
	public static final String USERNAME = "username";
	
	/** The Constant PASSWORD. */
	public static final String PASSWORD = "password";
	
	/** The Constant WEATHER_DATETIME_FORMAT. */
	public static final String WEATHER_DATETIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
	
	/** The Constant DATE_FORMAT. */
	public static final String DATE_FORMAT = "yyyy-MM-d";
	
	/** The Constant CITY. */
	public static final String CITY = "city";
	
	/** The Constant DESCRIPTION. */
	public static final String DESCRIPTION = "description";
	
	/** The Constant TEMP. */
	public static final String TEMP = "temp";
	
	/** The Constant TEMP_MIN. */
	public static final String TEMP_MIN = "temp_min";
	
	/** The Constant TEMP_MAX. */
	public static final String TEMP_MAX = "temp_max";
	
	/** The Constant WEATHER. */
	public static final String WEATHER = "weather";
	
	/** The Constant NAME. */
	public static final String NAME = "name";
	
	/** The Constant TIMEZONE. */
	public static final String TIMEZONE = "timezone";
	
	/** The Constant SUNRISE. */
	public static final String SUNRISE = "sunrise";
	
	/** The Constant SUNSET. */
	public static final String SUNSET = "sunset";
	
	/** The Constant MD5_ERR_CODE. */
	public static final String MD5_ERR_CODE = "2000";
	
	/** The Constant STATUS. */
	public static final String STATUS = "status";
	
	/** The Constant REGISTRATION_FAILED_MSG. */
	public static final String REGISTRATION_FAILED_MSG = "Registration API failed. Please check logs";
	
	/**
	 * Instantiates a new constant.
	 */
	private Constant() {
		super();
	}

}

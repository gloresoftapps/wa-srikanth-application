package com.weatherforecast;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The Class WeatherforecastApplication.
 */
@SpringBootApplication
public class WeatherforecastApplication {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(WeatherforecastApplication.class, args);
	}

	/**
	 * Instantiates a new weatherforecast application.
	 */
	public WeatherforecastApplication() {
		super();
	}

}

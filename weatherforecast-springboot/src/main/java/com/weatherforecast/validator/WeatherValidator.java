package com.weatherforecast.validator;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import org.apache.commons.validator.routines.BigDecimalValidator;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.weatherforecast.constant.Constant;
import com.weatherforecast.dto.request.WeatherRequest;
import com.weatherforecast.exception.BaseException;

/**
 * The Class WeatherValidator.
 */
@Component
public class WeatherValidator {

	/**
	 * Validate update weather.
	 *
	 * @param request the request
	 * @throws BaseException the base exception
	 */
	public void validateUpdateWeather(WeatherRequest request) throws BaseException {
		BigDecimal temperature = request.getTemperature();
		String sunRise = request.getSunRise();
		String sunSet = request.getSunSet();

		try {
			boolean isTemperatureValid = BigDecimalValidator.getInstance().isInRange(temperature, -88, 58);

			if (!isTemperatureValid) {
				throw new BaseException(Constant.UPDATE_WEATHER_ERR_CODE,
						"Invalid Temperature.Allowed range is between -88 celcius " + "and 58 celcius",
						HttpStatus.BAD_REQUEST);
			}
			LocalDateTime.parse(sunRise, DateTimeFormatter.ofPattern(Constant.WEATHER_DATETIME_FORMAT));
			LocalDateTime.parse(sunSet, DateTimeFormatter.ofPattern(Constant.WEATHER_DATETIME_FORMAT));
		} catch (DateTimeParseException e) {
			throw new BaseException(Constant.UPDATE_WEATHER_ERR_CODE,
					"Invalid Date Input. Accepted " + "format is yyyy-MM-ddTHH:mm:ss", HttpStatus.BAD_REQUEST, e);
		}
	}
}

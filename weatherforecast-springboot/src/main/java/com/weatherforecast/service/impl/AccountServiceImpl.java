package com.weatherforecast.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.weatherforecast.builder.AppRequestBuilder;
import com.weatherforecast.builder.AppResponseBuilder;
import com.weatherforecast.constant.Constant;
import com.weatherforecast.dao.AccountDAO;
import com.weatherforecast.dto.request.AccountRequest;
import com.weatherforecast.dto.request.CustomerRequest;
import com.weatherforecast.dto.response.AppResponse;
import com.weatherforecast.entity.Customer;
import com.weatherforecast.exception.BaseException;
import com.weatherforecast.service.IAccountService;

/**
 * The Class AccountServiceImpl.
 */
@Service
public class AccountServiceImpl implements IAccountService {
	
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(AccountServiceImpl.class);

	/** The request builder. */
	@Autowired
	private AppRequestBuilder requestBuilder;

	/** The account DAO. */
	@Autowired
	private AccountDAO accountDAO;

	/** The response builder. */
	@Autowired
	private AppResponseBuilder responseBuilder;

	/**
	 * Register account.
	 *
	 * @param request the request
	 * @return the app response
	 * @throws BaseException the base exception
	 */
	@Override
	public AppResponse registerAccount(AccountRequest request) throws BaseException {
		LOGGER.info("Entering the method registerAccount in service");
		AppResponse response = null;
		try {
			Customer customer = requestBuilder.constructCustomer(request);
			accountDAO.createCustomerEntity(customer);
			response = responseBuilder.constructResponse(Constant.REGISTER_SUCCESS_MSG, Boolean.TRUE);
			LOGGER.info("Exiting the method registerAccount in service");
		} catch (Exception e) {			
			throw new BaseException(Constant.REGISTRATION_ERROR_CODE, "Registration API failed. Please check logs.",
					HttpStatus.INTERNAL_SERVER_ERROR, e);
		}
		return response;
	}

	/**
	 * Gets the login info.
	 *
	 * @param customer the customer
	 * @return the login info
	 * @throws BaseException the base exception
	 */
	public Map<String,Object> getLoginInfo(CustomerRequest customer) throws BaseException {
		LOGGER.info("Entering the method getLoginInfo in service");
		Object response = null;
		HttpStatus httpStatus = null;
		Map<String,Object> responseMap = new HashMap<>();
		try {			
			String encodedPwd = requestBuilder.getMD5Password(customer.getPassword());
			Customer customerResponse = accountDAO.getCustomerLogin(customer.getUsername(), encodedPwd);
			
			if(null != customerResponse) {
				response = responseBuilder.constructCustomer(customerResponse);
				httpStatus = HttpStatus.OK;								
			}
			else {
				response = responseBuilder.constructResponse(Constant.LOGIN_FAILURE_MSG, Boolean.FALSE);
				httpStatus = HttpStatus.UNAUTHORIZED;
			}									
			responseMap.put("customer", response);
			responseMap.put("httpStatus", httpStatus);
			LOGGER.info("Exiting the method getLoginInfo in service");
		} catch (Exception e) {			
			throw new BaseException(Constant.LOGIN_API_ERROR, "Login API failed. Please check logs.",
					HttpStatus.INTERNAL_SERVER_ERROR, e);
		}
		return responseMap;
	}
}

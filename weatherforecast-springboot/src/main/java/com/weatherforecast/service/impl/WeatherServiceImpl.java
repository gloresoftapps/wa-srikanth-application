package com.weatherforecast.service.impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.weatherforecast.builder.AppRequestBuilder;
import com.weatherforecast.builder.AppResponseBuilder;
import com.weatherforecast.builder.JSONResponseBuilder;
import com.weatherforecast.config.WeatherConfig;
import com.weatherforecast.constant.Constant;
import com.weatherforecast.dao.WeatherDAO;
import com.weatherforecast.dto.request.DeleteRequest;
import com.weatherforecast.dto.request.SearchRequest;
import com.weatherforecast.dto.request.WeatherRequest;
import com.weatherforecast.dto.response.AppResponse;
import com.weatherforecast.dto.response.CustomerResponse;
import com.weatherforecast.dto.response.WeatherResponse;
import com.weatherforecast.entity.Customer;
import com.weatherforecast.entity.History;
import com.weatherforecast.entity.Weather;
import com.weatherforecast.exception.BaseException;
import com.weatherforecast.service.IWeatherService;
import com.weatherforecast.util.WeatherUtil;

/**
 * The Class WeatherServiceImpl.
 */
@Service
public class WeatherServiceImpl implements IWeatherService {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(WeatherServiceImpl.class);

	/** The request builder. */
	@Autowired
	private AppRequestBuilder requestBuilder;

	/** The weather DAO. */
	@Autowired
	private WeatherDAO weatherDAO;

	/** The response builder. */
	@Autowired
	private AppResponseBuilder responseBuilder;

	/** The util. */
	@Autowired
	private WeatherUtil util;

	/** The config. */
	@Autowired
	private WeatherConfig config;

	/** The json builder. */
	@Autowired
	private JSONResponseBuilder jsonBuilder;

	/**
	 * Read weather history.
	 *
	 * @param customerId the customer id
	 * @return the list
	 * @throws BaseException the base exception
	 */
	public List<WeatherResponse> readWeatherHistory(String customerId) throws BaseException {
		LOGGER.info("Entering the method readWeatherHistory in service");
		List<WeatherResponse> listWeatherResponse = null;
		try {
			List<Weather> listWeather = weatherDAO.readWeatherHistory(customerId);
			listWeatherResponse = responseBuilder.constructWeatherHistoryResponse(listWeather);
		} catch (Exception e) {			
			throw new BaseException(Constant.READWEATHERHISTORY_API_ERROR,
					"Read Weather History API failed. Please check logs.", HttpStatus.INTERNAL_SERVER_ERROR, e);
		}
		LOGGER.info("Exiting the method readWeatherHistory in service");
		return listWeatherResponse;
	}

	/**
	 * Update weather.
	 *
	 * @param request the request
	 * @param weatherId the weather id
	 * @return the app response
	 * @throws BaseException the base exception
	 */
	public AppResponse updateWeather(WeatherRequest request, String weatherId) throws BaseException {
		LOGGER.info("Entering the method updateWeather in service");
		AppResponse response = null;
		try {
			Weather weatherEntity = weatherDAO.getWeatherById(weatherId);

			if (null == weatherEntity) {
				throw new BaseException(Constant.UPDATE_WEATHER_ERR_CODE,
						"Weather not found.Please" + "check your input and try again.", HttpStatus.BAD_REQUEST);
			}

			weatherEntity = requestBuilder.getUpdateWeatherObject(request, weatherEntity);
			weatherDAO.updateWeatherEntity(weatherEntity);
			response = responseBuilder.constructResponse(Constant.UPDATE_WEATHER_SUCCESS_MSG, Boolean.TRUE);
		} catch (Exception e) {			
			throw new BaseException(Constant.UPDATE_WEATHER_ERR_CODE, "Update Weather API failed. Please check logs.",
					HttpStatus.INTERNAL_SERVER_ERROR, e);
		}
		LOGGER.info("Exiting the method updateWeather in service");
		return response;
	}

	/**
	 * Delete weather.
	 *
	 * @param weatherId the weather id
	 * @param request the request
	 * @return the app response
	 * @throws BaseException the base exception
	 */
	public AppResponse deleteWeather(String weatherId, DeleteRequest request) throws BaseException {
		LOGGER.info("Entering the method deleteWeather in service");
		AppResponse response = null;
		try {

			List<History> listHistoryEntity = weatherDAO.getHistoryByWeatherCustomerId(weatherId,
					request.getCustomerId());

			if (CollectionUtils.isNotEmpty(listHistoryEntity)) {
				weatherDAO.deleteHistoryEntity(listHistoryEntity);
				response = responseBuilder.constructResponse(Constant.DELETE_WEATHER_SUCCESS_MSG, Boolean.TRUE);
			}
			else {
				response = responseBuilder.constructResponse(Constant.WEATHER_NOT_FOUND, Boolean.TRUE);
			}
		} catch (Exception e) {			
			throw new BaseException(Constant.DELETE_WEATHER_ERR_CODE, "Delete Weather API failed. Please check logs.",
					HttpStatus.INTERNAL_SERVER_ERROR, e);
		}
		LOGGER.info("Exiting the method deleteWeather in service");
		return response;
	}

	/**
	 * Delete bulk weather.
	 *
	 * @param deleteRequest the delete request
	 * @return the app response
	 * @throws BaseException the base exception
	 */
	public AppResponse deleteBulkWeather(DeleteRequest deleteRequest) throws BaseException {
		LOGGER.info("Entering the method deleteBulkWeather in service");
		AppResponse response = null;
		try {
			
			List<History> listHistoryEntity = weatherDAO.getBulkHistory(deleteRequest.getCustomerId(),
					deleteRequest.getWeathers());

			if (CollectionUtils.isNotEmpty(listHistoryEntity)) {
				weatherDAO.deleteHistoryEntity(listHistoryEntity);
				response = responseBuilder.constructResponse(Constant.DELETE_WEATHER_SUCCESS_MSG, Boolean.TRUE);
			}
			else {
				response = responseBuilder.constructResponse(Constant.WEATHER_NOT_FOUND, Boolean.TRUE);
			}
		} catch (Exception e) {			
			throw new BaseException(Constant.DELETEBULK_WEATHER_ERR_CODE,
					"Delete Weather API failed. Please check logs.", HttpStatus.INTERNAL_SERVER_ERROR, e);
		}
		LOGGER.info("Exiting the method deleteBulkWeather in service");

		return response;
	}

	/**
	 * Search customers by city.
	 *
	 * @param city the city
	 * @return the list
	 * @throws BaseException the base exception
	 */
	public List<CustomerResponse> searchCustomersByCity(String city) throws BaseException {
		LOGGER.info("Entering the method searchCustomersByCity in service");
		List<CustomerResponse> listCustomerResponse = null;
		try {
			List<Customer> customers = weatherDAO.getCustomersByCity(city.toLowerCase());
			listCustomerResponse = responseBuilder.constructCustomers(customers);
		} catch (Exception e) {
			throw new BaseException(Constant.CUSTOMERS_BYCITY_ERR_CODE,
					"Search Customers By City API failed. Please check logs.", HttpStatus.INTERNAL_SERVER_ERROR, e);
		}
		LOGGER.info("Exiting the method searchCustomersByCity in service");
		return listCustomerResponse;

	}

	/**
	 * Search cities by customer.
	 *
	 * @param customerId the customer id
	 * @return the list
	 * @throws BaseException the base exception
	 */
	public List<WeatherResponse> searchCitiesByCustomer(String customerId) throws BaseException {
		LOGGER.info("Entering the method searchCitiesByCustomer in service");
		List<WeatherResponse> listWeatherResponse = null;
		try {
			List<Weather> weathers = weatherDAO.getCitiesByCustomer(customerId);
			listWeatherResponse = responseBuilder.constructWeatherHistoryResponse(weathers);
		} catch (Exception e) {
			throw new BaseException(Constant.CITIES_BYCUSTOMER_ERR_CODE,
					"Search Cities By Customer API failed. Please check logs.", HttpStatus.INTERNAL_SERVER_ERROR, e);
		}
		LOGGER.info("Exiting the method searchCitiesByCustomer in service");
		return listWeatherResponse;
	}

	/**
	 * Search weather by city.
	 *
	 * @param request the request
	 * @param customerId the customer id
	 * @return the weather response
	 * @throws BaseException the base exception
	 */
	public WeatherResponse searchWeatherByCity(SearchRequest request, String customerId) throws BaseException {
		LOGGER.info("Entering the method searchWeatherByCity in service");
		WeatherResponse weather = null;
		try {
			String searchWeatherURL = util.formatMessage(config.getSearchWeatherURL(), request.getCity());
			ResponseEntity<String> response = util.restExchange(searchWeatherURL, HttpMethod.GET, null, String.class);

			if (null != response && HttpStatus.OK == response.getStatusCode()) {
				weather = jsonBuilder.constructWeatherObject(response.getBody());
			}			
			saveWeatherHistory(request.getName(), weather, customerId);
		} catch (Exception e) {
			throw new BaseException(Constant.WEATHER_BYCITY_ERR_CODE,
					"Search Weather By City API failed. Please check logs.", HttpStatus.INTERNAL_SERVER_ERROR, e);
		}
		LOGGER.info("Exiting the method searchWeatherByCity in service");
		return weather;

	}

	/**
	 * Save weather history.
	 *
	 * @param userName the user name
	 * @param weatherObj the weather obj
	 * @param customerId the customer id
	 */
	public void saveWeatherHistory(String userName, WeatherResponse weatherObj, String customerId) {
		LOGGER.info("Entering the method saveWeatherHistory in service");
		 CompletableFuture.runAsync(() -> {

		try {
			String inputCity = weatherObj.getCity().toLowerCase();
			Weather weather = null;
			LocalDateTime createdDate = LocalDateTime.now();
			try {
				weather = weatherDAO.searchWeatherByCity(inputCity);
			} catch (Exception e) {
				LOGGER.info("Exception occured as the entity is not available for the first occurence");
			}

			if (null != weather && StringUtils.isNotBlank(weather.getCity())) {
				LOGGER.info("weather already exists hence skipping to add it and adding only history in db");
				Customer customer = new Customer();
				customer.setId(customerId);
				History history = new History();
				history.setCreatedDate(createdDate);
				history.setWeather(weather);
				history.setCustomer(customer);
				weatherDAO.createHistoryEntity(history);
			} else {
				List<Weather> listWeather = new ArrayList<>();
				Weather weatherEntity = new Weather();
				weatherEntity.setCity(inputCity);
				weatherEntity.setDescription(weatherObj.getDescription());
				weatherEntity.setTemperature(weatherObj.getTemperature());
				weatherEntity.setMinTemperature(weatherObj.getMinTemperature());
				weatherEntity.setMaxTemperature(weatherObj.getMaxTemperature());
				weatherEntity.setSunRise(util.convertDateStrToDate(weatherObj.getSunRise()));
				weatherEntity.setSunSet(util.convertDateStrToDate(weatherObj.getSunSet()));

				Customer customer = new Customer();
				customer.setId(customerId);
				History history = new History();
				history.setCreatedDate(createdDate);
				history.setWeather(weatherEntity);
				history.setCustomer(customer);
				listWeather.add(weatherEntity);
				weatherDAO.createWeatherEntity(listWeather);
				weatherDAO.createHistoryEntity(history);
			}
			
			LOGGER.info("Exiting the method saveWeatherHistory in service");
		} catch (Exception e) {			
			LOGGER.error("Exception while saving weather history for user {}", userName);
		}
		 });
	}
}

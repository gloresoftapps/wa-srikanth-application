package com.weatherforecast.service;

import java.util.Map;

import com.weatherforecast.dto.request.AccountRequest;
import com.weatherforecast.dto.request.CustomerRequest;
import com.weatherforecast.dto.response.AppResponse;
import com.weatherforecast.exception.BaseException;

/**
 * The Interface IAccountService.
 */
public interface IAccountService {
	
	/**
	 * Register account.
	 *
	 * @param request the request
	 * @return the app response
	 * @throws BaseException the base exception
	 */
	AppResponse registerAccount(AccountRequest request) throws BaseException;
	
	
	/**
	 * Gets the login info.
	 *
	 * @param customer the customer
	 * @return the login info
	 * @throws BaseException the base exception
	 */
	Map<String,Object> getLoginInfo(CustomerRequest customer) throws BaseException;

}

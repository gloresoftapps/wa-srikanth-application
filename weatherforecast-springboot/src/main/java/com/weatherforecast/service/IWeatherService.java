package com.weatherforecast.service;

import java.util.List;

import com.weatherforecast.dto.request.DeleteRequest;
import com.weatherforecast.dto.request.SearchRequest;
import com.weatherforecast.dto.request.WeatherRequest;
import com.weatherforecast.dto.response.AppResponse;
import com.weatherforecast.dto.response.CustomerResponse;
import com.weatherforecast.dto.response.WeatherResponse;
import com.weatherforecast.exception.BaseException;

/**
 * The Interface IWeatherService.
 */
public interface IWeatherService {
	 
	 /**
 	 * Read weather history.
 	 *
 	 * @param customerId the customer id
 	 * @return the list
 	 * @throws BaseException the base exception
 	 */
 	List<WeatherResponse> readWeatherHistory(String customerId) throws BaseException;
	 
	 /**
 	 * Update weather.
 	 *
 	 * @param weatherRequest the weather request
 	 * @param weatherId the weather id
 	 * @return the app response
 	 * @throws BaseException the base exception
 	 */
 	AppResponse updateWeather(WeatherRequest weatherRequest, String weatherId) throws BaseException;
	 
	 /**
 	 * Delete weather.
 	 *
 	 * @param weatherId the weather id
 	 * @param deleteRequest the delete request
 	 * @return the app response
 	 * @throws BaseException the base exception
 	 */
 	AppResponse deleteWeather(String weatherId, DeleteRequest deleteRequest) throws BaseException;
	 
	 /**
 	 * Delete bulk weather.
 	 *
 	 * @param deleteRequest the delete request
 	 * @return the app response
 	 * @throws BaseException the base exception
 	 */
 	AppResponse deleteBulkWeather(DeleteRequest deleteRequest) throws BaseException;
	 
	 /**
 	 * Search customers by city.
 	 *
 	 * @param city the city
 	 * @return the list
 	 * @throws BaseException the base exception
 	 */
 	List<CustomerResponse> searchCustomersByCity(String city) throws BaseException;
	 
	 /**
 	 * Search cities by customer.
 	 *
 	 * @param customerId the customer id
 	 * @return the list
 	 * @throws BaseException the base exception
 	 */
 	List<WeatherResponse> searchCitiesByCustomer(String customerId) throws BaseException;
	 
	 /**
 	 * Search weather by city.
 	 *
 	 * @param request the request
 	 * @param customerId the customer id
 	 * @return the weather response
 	 * @throws BaseException the base exception
 	 */
 	WeatherResponse searchWeatherByCity(SearchRequest request, String customerId) throws BaseException;

}

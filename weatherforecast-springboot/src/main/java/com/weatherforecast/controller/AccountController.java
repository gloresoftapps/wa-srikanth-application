package com.weatherforecast.controller;

import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.weatherforecast.dto.request.AccountRequest;
import com.weatherforecast.dto.request.CustomerRequest;
import com.weatherforecast.dto.response.AppResponse;
import com.weatherforecast.exception.BaseException;
import com.weatherforecast.service.IAccountService;

/**
 * The Class AccountController.
 */
@RestController
@RequestMapping("/accounts/v1")
@Validated
@CrossOrigin("*")
public class AccountController {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(AccountController.class);

	/** The account service. */
	@Autowired
	private IAccountService accountService;

	/**
	 * Instantiates a new account controller.
	 */
	public AccountController() {
		super();
	}

	/**
	 * Register account.
	 *
	 * @param request the request
	 * @return the response entity
	 * @throws BaseException the base exception
	 */
	@PostMapping(path = "/register", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AppResponse> registerAccount(@RequestBody @Valid AccountRequest request)
			throws BaseException {
		LOGGER.info("Reached method registerAccount in controller");
		AppResponse response = accountService.registerAccount(request);
		return new ResponseEntity<>(response, HttpStatus.CREATED);
	}

	/**
	 * Login customer.
	 *
	 * @param customer the customer
	 * @return the response entity
	 * @throws BaseException the base exception
	 */
	@PostMapping(path = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> loginCustomer(@RequestBody @Valid CustomerRequest customer) throws BaseException {
		LOGGER.info("Reached method loginCustomer in controller");		
		Map<String,Object> response = accountService.getLoginInfo(customer);		
		Object responseObj = response.getOrDefault("customer", new Object());
		HttpStatus httpStatus = (HttpStatus) response.getOrDefault("httpStatus", HttpStatus.OK);
		return new ResponseEntity<>(responseObj, httpStatus);
	}
}

package com.weatherforecast.controller;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.weatherforecast.dto.request.DeleteRequest;
import com.weatherforecast.dto.request.SearchRequest;
import com.weatherforecast.dto.request.WeatherRequest;
import com.weatherforecast.dto.response.AppResponse;
import com.weatherforecast.dto.response.CustomerResponse;
import com.weatherforecast.dto.response.WeatherResponse;
import com.weatherforecast.exception.BaseException;
import com.weatherforecast.service.IWeatherService;
import com.weatherforecast.validator.WeatherValidator;

/**
 * The Class WeatherController.
 */
@RestController
@RequestMapping("/weather/v1")
@CrossOrigin("*")
@Validated
public class WeatherController {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(WeatherController.class);

	/** The weather service. */
	@Autowired
	private IWeatherService weatherService;
	
	/** The weather validator. */
	@Autowired
	private WeatherValidator weatherValidator;

	/**
	 * Instantiates a new weather controller.
	 */
	public WeatherController() {
		super();
	}

	/**
	 * Read weather history.
	 *
	 * @param customerId the customer id
	 * @return the response entity
	 * @throws BaseException the base exception
	 */
	@GetMapping(path = "/history/{customerId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<WeatherResponse>> readWeatherHistory(@PathVariable String customerId)
			throws BaseException {
		LOGGER.info("Reached method readWeatherHistory in controller");
		return new ResponseEntity<>(weatherService.readWeatherHistory(customerId), HttpStatus.OK);
	}

	/**
	 * Update weather.
	 *
	 * @param request the request
	 * @param weatherId the weather id
	 * @return the response entity
	 * @throws BaseException the base exception
	 */
	@PutMapping(path = "/update/{weatherId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AppResponse> updateWeather(@RequestBody @Valid WeatherRequest request,
			@PathVariable String weatherId) throws BaseException {
		LOGGER.info("Reached method updateWeather in controller");
		weatherValidator.validateUpdateWeather(request);
		return new ResponseEntity<>(weatherService.updateWeather(request, weatherId), HttpStatus.OK);
	}

	/**
	 * Delete weather.
	 *
	 * @param weatherId the weather id
	 * @param request the request
	 * @return the response entity
	 * @throws BaseException the base exception
	 */
	@PostMapping(path = "/delete/{weatherId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AppResponse> deleteWeather(@PathVariable String weatherId,
			@RequestBody @Valid DeleteRequest request) throws BaseException {
		LOGGER.info("Reached method deleteWeather in controller");
		return new ResponseEntity<>(weatherService.deleteWeather(weatherId, request), HttpStatus.OK);
	}

	/**
	 * Delete bulk weather.
	 *
	 * @param deleteRequest the delete request
	 * @return the response entity
	 * @throws BaseException the base exception
	 */
	@PostMapping(path = "/delete/bulk", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AppResponse> deleteBulkWeather(@RequestBody @Valid DeleteRequest deleteRequest) 
			throws BaseException {
		LOGGER.info("Reached method deleteBulkWeather in controller");
		return new ResponseEntity<>(weatherService.deleteBulkWeather(deleteRequest), HttpStatus.OK);
	}

	/**
	 * Search customers by city.
	 *
	 * @param city the city
	 * @return the response entity
	 * @throws BaseException the base exception
	 */
	@GetMapping(path = "/search/customers", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<CustomerResponse>> searchCustomersByCity(@RequestParam @NotEmpty String city)
			throws BaseException {
		LOGGER.info("Reached method searchCustomersByCity in controller");
		return new ResponseEntity<>(weatherService.searchCustomersByCity(city), HttpStatus.OK);
	}

	/**
	 * Search cities by customer.
	 *
	 * @param customerId the customer id
	 * @return the response entity
	 * @throws BaseException the base exception
	 */
	@GetMapping(path = "/search/cities", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<WeatherResponse>> searchCitiesByCustomer(@RequestParam @NotEmpty String customerId)
			throws BaseException {
		LOGGER.info("Reached method searchCitiesByCustomer in controller");
		return new ResponseEntity<>(weatherService.searchCitiesByCustomer(customerId), HttpStatus.OK);
	}

	/**
	 * Search weather by city.
	 *
	 * @param request the request
	 * @param customerId the customer id
	 * @return the response entity
	 * @throws BaseException the base exception
	 */
	@PostMapping(path = "/search/{customerId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<WeatherResponse> searchWeatherByCity(@RequestBody @Valid SearchRequest request,
			@PathVariable String customerId) throws BaseException {
		LOGGER.info("Reached method searchWeatherByCity in controller");
		return new ResponseEntity<>(weatherService.searchWeatherByCity(request, customerId), HttpStatus.OK);
	}
}

package com.weatherforecast.util;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

/**
 * The Class TemperatureSerializer.
 */
public class TemperatureSerializer extends JsonSerializer<BigDecimal>{

	/**
	 * Serialize.
	 *
	 * @param value the value
	 * @param gen the gen
	 * @param serializers the serializers
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Override
	public void serialize(BigDecimal value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
		gen.writeNumber(value.setScale(2, RoundingMode.HALF_UP));		
	}
}

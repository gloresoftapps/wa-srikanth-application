
package com.weatherforecast.util;

import java.text.MessageFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.weatherforecast.constant.Constant;

/**
 * The Class WeatherUtil.
 */
@Component
public class WeatherUtil {

	/** The rest template. */
	@Autowired
	private RestTemplate restTemplate;

	/**
	 * Parses the date.
	 *
	 * @param date the date
	 * @return the local date
	 */
	public LocalDate parseDate(String date) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(Constant.DATE_FORMAT);
		return LocalDate.parse(date, formatter);
	}

	/**
	 * Convert long time zone to date.
	 *
	 * @param date the date
	 * @param timezone the timezone
	 * @return the local date time
	 */
	public LocalDateTime convertLongTimeZoneToDate(Long date, int timezone) {
		ZoneOffset offset = ZoneOffset.ofTotalSeconds(timezone);
		ZonedDateTime zonedDate = Instant.ofEpochSecond(date).atOffset(offset).toZonedDateTime();
		return zonedDate.toLocalDateTime();
	}

	/**
	 * Format local date time to string.
	 *
	 * @param localDateTimeObj the local date time obj
	 * @return the string
	 */
	public String formatLocalDateTimeToString(LocalDateTime localDateTimeObj) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(Constant.WEATHER_DATETIME_FORMAT);
		return localDateTimeObj.format(formatter);
	}

	/**
	 * Convert date str to date.
	 *
	 * @param dateStr the date str
	 * @return the local date time
	 */
	public LocalDateTime convertDateStrToDate(String dateStr) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(Constant.WEATHER_DATETIME_FORMAT);
		return LocalDateTime.parse(dateStr, formatter);
	}

	/**
	 * Rest exchange.
	 *
	 * @param query the query
	 * @param httpMethod the http method
	 * @param httpEntity the http entity
	 * @param responseType the response type
	 * @return the response entity
	 */
	public ResponseEntity<String> restExchange(String query, HttpMethod httpMethod, HttpEntity<?> httpEntity,
			Class<String> responseType) {
		return restTemplate.exchange(query, httpMethod, httpEntity, responseType);
	}

	/**
	 * Format message.
	 *
	 * @param url the url
	 * @param args the args
	 * @return the string
	 */
	public String formatMessage(String url, String... args) {
		return MessageFormat.format(url, args);
	}
}

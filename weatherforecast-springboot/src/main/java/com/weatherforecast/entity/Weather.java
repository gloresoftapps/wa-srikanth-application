package com.weatherforecast.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.GenericGenerator;

/**
 * The Class Weather.
 */
@Entity
@Table(name="weather", schema="public",uniqueConstraints = {
        @UniqueConstraint(columnNames = "city") })
public class Weather {
	
	/** The id. */
	@Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid", strategy = "uuid")
    private String id;
	
	/** The city. */
	@Column(name = "city", unique = true, nullable = false)
	private String city;
	
	/** The description. */
	@Column(name = "description", nullable = false)
	private String description;
	
	/** The temperature. */
	@Column(name = "temperature", nullable = false, precision=5, scale=2)
	private BigDecimal temperature;
	
	/** The min temperature. */
	@Column(name = "mintemperature", nullable = false, precision=5, scale=2)
	private BigDecimal minTemperature;
	
	/** The max temperature. */
	@Column(name = "maxtemperature", nullable = false, precision=5, scale=2)
	private BigDecimal maxTemperature;
	
	/** The sun rise. */
	@Column(name = "sunrise", nullable = false)
	private LocalDateTime sunRise;
	
	/** The sun set. */
	@Column(name = "sunset", nullable = false)
	private LocalDateTime sunSet;
	

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the city.
	 *
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Sets the city.
	 *
	 * @param city the new city
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the temperature.
	 *
	 * @return the temperature
	 */
	public BigDecimal getTemperature() {
		return temperature;
	}

	/**
	 * Sets the temperature.
	 *
	 * @param temperature the new temperature
	 */
	public void setTemperature(BigDecimal temperature) {
		this.temperature = temperature;
	}

	/**
	 * Gets the min temperature.
	 *
	 * @return the min temperature
	 */
	public BigDecimal getMinTemperature() {
		return minTemperature;
	}

	/**
	 * Sets the min temperature.
	 *
	 * @param minTemperature the new min temperature
	 */
	public void setMinTemperature(BigDecimal minTemperature) {
		this.minTemperature = minTemperature;
	}

	/**
	 * Gets the max temperature.
	 *
	 * @return the max temperature
	 */
	public BigDecimal getMaxTemperature() {
		return maxTemperature;
	}

	/**
	 * Sets the max temperature.
	 *
	 * @param maxTemperature the new max temperature
	 */
	public void setMaxTemperature(BigDecimal maxTemperature) {
		this.maxTemperature = maxTemperature;
	}

	/**
	 * Gets the sun rise.
	 *
	 * @return the sun rise
	 */
	public LocalDateTime getSunRise() {
		return sunRise;
	}

	/**
	 * Sets the sun rise.
	 *
	 * @param sunRise the new sun rise
	 */
	public void setSunRise(LocalDateTime sunRise) {
		this.sunRise = sunRise;
	}

	/**
	 * Gets the sun set.
	 *
	 * @return the sun set
	 */
	public LocalDateTime getSunSet() {
		return sunSet;
	}

	/**
	 * Sets the sun set.
	 *
	 * @param sunSet the new sun set
	 */
	public void setSunSet(LocalDateTime sunSet) {
		this.sunSet = sunSet;
	}
	
}

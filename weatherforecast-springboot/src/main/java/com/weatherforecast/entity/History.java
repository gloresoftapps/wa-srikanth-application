
package com.weatherforecast.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * The Class History.
 */
@Entity
@Table(name="history", schema="public")
public class History {
	
	/** The id. */
	@Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid", strategy = "uuid")
    private String id;	
	
	/** The weather. */
	@OneToOne
	@JoinColumn(name="weatherid")
	private Weather weather;
	
	/** The customer. */
	@OneToOne
	@JoinColumn(name="customerid")
	private Customer customer;
	
	/** The created date. */
	@Column(name="createddate")
	private LocalDateTime createdDate;

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the weather.
	 *
	 * @return the weather
	 */
	public Weather getWeather() {
		return weather;
	}

	/**
	 * Sets the weather.
	 *
	 * @param weather the new weather
	 */
	public void setWeather(Weather weather) {
		this.weather = weather;
	}

	/**
	 * Gets the customer.
	 *
	 * @return the customer
	 */
	public Customer getCustomer() {
		return customer;
	}

	/**
	 * Sets the customer.
	 *
	 * @param customer the new customer
	 */
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	/**
	 * Gets the created date.
	 *
	 * @return the created date
	 */
	public LocalDateTime getCreatedDate() {
		return createdDate;
	}

	/**
	 * Sets the created date.
	 *
	 * @param createdDate the new created date
	 */
	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}

}

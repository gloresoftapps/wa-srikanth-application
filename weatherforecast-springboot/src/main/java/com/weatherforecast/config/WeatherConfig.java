package com.weatherforecast.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * The Class WeatherConfig.
 */
@Configuration
@ConfigurationProperties
@PropertySource("classpath:application.properties")
public class WeatherConfig {

	/** The search weather URL. */
	private String searchWeatherURL;

	/**
	 * Gets the search weather URL.
	 *
	 * @return the search weather URL
	 */
	public String getSearchWeatherURL() {
		return searchWeatherURL;
	}

	/**
	 * Sets the search weather URL.
	 *
	 * @param searchWeatherURL the new search weather URL
	 */
	public void setSearchWeatherURL(String searchWeatherURL) {
		this.searchWeatherURL = searchWeatherURL;
	}
}

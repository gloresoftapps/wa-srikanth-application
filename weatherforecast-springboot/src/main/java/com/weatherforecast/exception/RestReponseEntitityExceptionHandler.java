package com.weatherforecast.exception;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.weatherforecast.constant.Constant;
import com.weatherforecast.dto.response.AppResponse;

/**
 * The Class RestReponseEntitityExceptionHandler.
 */
@RestControllerAdvice
public class RestReponseEntitityExceptionHandler extends ResponseEntityExceptionHandler {

	/**
	 * Instantiates a new rest reponse entitity exception handler.
	 */
	public RestReponseEntitityExceptionHandler() {
		super();
	}

	/**
	 * Handle base exception.
	 *
	 * @param ex      the ex
	 * @param request the request
	 * @return the response entity
	 */
	@ExceptionHandler(BaseException.class)
	public ResponseEntity<Object> handleBaseException(BaseException ex, WebRequest request) {
		HttpStatus status = ex.getHttpStatus();
		return handleExceptionInternal(ex, constructResponse(ex.getMessage(), status.value(), ex.getErrorCode()),
				new HttpHeaders(), status, request);
	}

	/**
	 * Handle json processing exception.
	 *
	 * @param ex      the ex
	 * @param request the request
	 * @return the response entity
	 */
	@ExceptionHandler(JsonProcessingException.class)
	public ResponseEntity<Object> handleJsonProcessingException(JsonProcessingException ex, WebRequest request) {
		return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR,
				request);
	}

	/**
	 * Construct response.
	 *
	 * @param message   the message
	 * @param status    the status
	 * @param errorCode the error code
	 * @return the string
	 */
	public Map<String, Object> constructResponse(String message, int status, String errorCode) {
		Map<String, Object> errorObj = new HashMap<>();
		Map<String, String> statusInfo = new HashMap<>();
		statusInfo.put(Constant.STATUS_CODE, String.valueOf(status));
		statusInfo.put(Constant.MESSAGE, message);
		statusInfo.put(Constant.ERROR_CODE, errorCode);
		errorObj.put(Constant.ERROR, statusInfo);
		return errorObj;
	}

	/**
	 * Handle constraint violation exceptions.
	 *
	 * @param ex the ex
	 * @return the response entity
	 */
	@ExceptionHandler(ConstraintViolationException.class)
	public final ResponseEntity<Object> handleConstraintViolationExceptions(ConstraintViolationException ex) {
		String exceptionResponse = String.format("Invalid input parameters: %s", ex.getMessage());
		return new ResponseEntity<>(exceptionResponse, HttpStatus.BAD_REQUEST);
	}

	/**
	 * Handle DAO constraint violation exceptions.
	 *
	 * @param ex the ex
	 * @return the response entity
	 */
	@ExceptionHandler(org.hibernate.exception.ConstraintViolationException.class)
	public final ResponseEntity<Object> handleDAOConstraintViolationExceptions(ConstraintViolationException ex) {
		String exceptionResponse = "City already exists and is a unique constraint. Please try with another city";
		return new ResponseEntity<>(exceptionResponse, HttpStatus.FORBIDDEN);
	}

	/**
	 * Construct response.
	 *
	 * @param message       the message
	 * @param status        the status
	 * @param errorCode     the error code
	 * @param logTrackingId the log tracking id
	 * @return the string
	 */
	public Map<String, Object> constructResponse(String message, int status, String errorCode, String logTrackingId) {
		Map<String, Object> errorObj = new HashMap<>();
		Map<String, String> statusInfo = new HashMap<>();
		statusInfo.put(Constant.STATUS_CODE, String.valueOf(status));
		statusInfo.put(Constant.MESSAGE, message);
		statusInfo.put(Constant.ERROR_CODE, errorCode);
		statusInfo.put(Constant.LOG_TRACKING_ID, logTrackingId);
		errorObj.put(Constant.ERROR, statusInfo);
		return errorObj;
	}

	
	/**
	 * Handle method argument not valid.
	 *
	 * @param ex the ex
	 * @param headers the headers
	 * @param status the status
	 * @param request the request
	 * @return the response entity
	 */
	@Override
	public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		AppResponse responseObj = new AppResponse();
		BindingResult result = ex.getBindingResult();
		List<ObjectError> listObjectError = result.getAllErrors();
		ObjectError objError = listObjectError.get(0);
		String message = objError.getDefaultMessage();
		responseObj.setMessage(message);
		responseObj.setSuccess(Boolean.FALSE);
		return new ResponseEntity<>(responseObj, HttpStatus.BAD_REQUEST);
	}
}

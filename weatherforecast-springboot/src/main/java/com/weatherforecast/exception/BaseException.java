package com.weatherforecast.exception;

import org.springframework.http.HttpStatus;

/**
 * The Class BaseException.
 */
public class BaseException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 9057181686459158924L;
	
	/** The Constant ERROR_CODE. */
	private static final String ERROR_CODE = "Error Code: ";
	
	/** The Constant ERROR_DETAIL. */
	private static final String ERROR_DETAIL = "Error Detail: ";

	/** The error code. */
	private final String errorCode;

	/** The exception. */
	private final Exception exception;

	/** The http status. */
	private final HttpStatus httpStatus;

	/**
	 * Instantiates a new base exception.
	 *
	 * @param errorMessage the error message
	 */
	public BaseException(String errorMessage) {
		super(ERROR_DETAIL.concat(errorMessage));
		this.errorCode = "";
		this.exception = new Exception();
		this.httpStatus = null;
	}

	/**
	 * Instantiates a new base exception.
	 *
	 * @param exception the exception
	 */
	public BaseException(Exception exception) {
		this.errorCode = "";
		this.exception = exception;
		this.httpStatus = null;

	}

	/**
	 * Instantiates a new base exception.
	 *
	 * @param errorMessage the error message
	 * @param errorCode the error code
	 */
	public BaseException(String errorMessage, String errorCode) {
		super(ERROR_CODE.concat(errorCode).concat(" ").concat(ERROR_DETAIL).concat(errorMessage));
		this.errorCode = "";
		this.exception = null;
		this.httpStatus = null;
	}

	/**
	 * Instantiates a new base exception.
	 *
	 * @param errorMessage the error message
	 * @param errorCode the error code
	 * @param exception the exception
	 */
	public BaseException(String errorMessage, String errorCode, Exception exception) {
		super(ERROR_CODE.concat(errorCode).concat(" ").concat(ERROR_DETAIL).concat(errorMessage), exception);
		this.errorCode = "";
		this.exception = exception;
		this.httpStatus = null;
	}

	/**
	 * Instantiates a new base exception.
	 *
	 * @param errorCode the error code
	 * @param errorMessage the error message
	 * @param httpStatus the http status
	 */
	public BaseException(String errorCode, String errorMessage, HttpStatus httpStatus) {
		super(ERROR_CODE.concat(errorCode).concat(" ").concat(ERROR_DETAIL).concat(errorMessage));
		this.errorCode = errorCode;
		this.httpStatus = httpStatus;
		this.exception = new Exception();
	}

	/**
	 * Instantiates a new base exception.
	 *
	 * @param errorCode the error code
	 * @param errorMessage the error message
	 * @param httpStatus the http status
	 * @param exception the exception
	 */
	public BaseException(String errorCode, String errorMessage, HttpStatus httpStatus, Exception exception) {
		super(ERROR_DETAIL.concat(errorMessage), exception);
		this.errorCode = errorCode;
		this.httpStatus = httpStatus;
		this.exception = exception;
	}

	/**
	 * Gets the error code.
	 *
	 * @return the error code
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * Gets the exception.
	 *
	 * @return the exception
	 */
	public Exception getException() {
		return exception;
	}

	/**
	 * Gets the http status.
	 *
	 * @return the http status
	 */
	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

}

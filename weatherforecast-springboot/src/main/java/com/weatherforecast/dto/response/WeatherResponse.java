package com.weatherforecast.dto.response;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * The Class WeatherResponse.
 */
@JsonInclude(Include.NON_NULL)
public class WeatherResponse {
	
	/** The weather id. */
	private String weatherId;
	
	/** The city. */
	private String city;
	
	/** The description. */
	private String description;
	
	/** The sun rise. */
	private String sunRise;
	
	/** The sun set. */
	private String sunSet;
	
	/** The temperature. */
	private BigDecimal temperature;
	
	/** The min temperature. */
	private BigDecimal minTemperature;
	
	/** The max temperature. */
	private BigDecimal maxTemperature;
 
	/**
	 * Gets the weather id.
	 *
	 * @return the weather id
	 */
	public String getWeatherId() {
		return weatherId;
	}

	/**
	 * Sets the weather id.
	 *
	 * @param weatherId the new weather id
	 */
	public void setWeatherId(String weatherId) {
		this.weatherId = weatherId;
	}

	/**
	 * Gets the city.
	 *
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Sets the city.
	 *
	 * @param city the new city
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the sun rise.
	 *
	 * @return the sun rise
	 */
	public String getSunRise() {
		return sunRise;
	}

	/**
	 * Sets the sun rise.
	 *
	 * @param sunRise the new sun rise
	 */
	public void setSunRise(String sunRise) {
		this.sunRise = sunRise;
	}

	/**
	 * Gets the sun set.
	 *
	 * @return the sun set
	 */
	public String getSunSet() {
		return sunSet;
	}

	/**
	 * Sets the sun set.
	 *
	 * @param sunSet the new sun set
	 */
	public void setSunSet(String sunSet) {
		this.sunSet = sunSet;
	}

	/**
	 * Gets the temperature.
	 *
	 * @return the temperature
	 */
	public BigDecimal getTemperature() {
		return temperature;
	}

	/**
	 * Sets the temperature.
	 *
	 * @param temperature the new temperature
	 */
	public void setTemperature(BigDecimal temperature) {
		this.temperature = temperature;
	}

	/**
	 * Gets the min temperature.
	 *
	 * @return the min temperature
	 */
	public BigDecimal getMinTemperature() {
		return minTemperature;
	}

	/**
	 * Sets the min temperature.
	 *
	 * @param minTemperature the new min temperature
	 */
	public void setMinTemperature(BigDecimal minTemperature) {
		this.minTemperature = minTemperature;
	}

	/**
	 * Gets the max temperature.
	 *
	 * @return the max temperature
	 */
	public BigDecimal getMaxTemperature() {
		return maxTemperature;
	}

	/**
	 * Sets the max temperature.
	 *
	 * @param maxTemperature the new max temperature
	 */
	public void setMaxTemperature(BigDecimal maxTemperature) {
		this.maxTemperature = maxTemperature;
	}
	
}

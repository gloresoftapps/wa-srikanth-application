package com.weatherforecast.dto.request;

import javax.validation.constraints.NotEmpty;

/**
 * The Class CustomerRequest.
 */
public class CustomerRequest {

	/** The username. */
	@NotEmpty(message = "username must not be empty")
	private String username;

	/** The password. */
	@NotEmpty(message = "password must not be empty")
	private String password;

	/**
	 * Gets the username.
	 *
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Sets the username.
	 *
	 * @param username the new username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the password.
	 *
	 * @param password the new password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

}

package com.weatherforecast.dto.request;

import java.math.BigDecimal;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.weatherforecast.util.TemperatureSerializer;

/**
 * The Class WeatherRequest.
 */
public class WeatherRequest {
	
	/** The city. */
	private String city;
	
	/** The description. */
	private String description;
	
	/** The temperature. */
	@JsonProperty("temperature")
	@JsonSerialize(using=TemperatureSerializer.class)
	@NotNull(message="temperature must not be null")
	private BigDecimal temperature;
	
	/** The min temperature. */
	@JsonProperty("minTemperature")
	@JsonSerialize(using=TemperatureSerializer.class)
	private BigDecimal minTemperature;
	
	/** The max temperature. */
	@JsonProperty("maxTemperature")
	@JsonSerialize(using=TemperatureSerializer.class)
	private BigDecimal maxTemperature;
	
	/** The sun set. */
	@NotEmpty(message="sunSet must not be empty")
	private String sunSet;
	
	/** The sun rise. */
	@NotEmpty(message="sunRise must not be empty")
	private String sunRise;
	
	/** The timezone. */
	private int timezone;
	
	/** The customer id. */
	private String customerId;

	/**
	 * Gets the city.
	 *
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Sets the city.
	 *
	 * @param city the new city
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the temperature.
	 *
	 * @return the temperature
	 */
	public BigDecimal getTemperature() {
		return temperature;
	}

	/**
	 * Sets the temperature.
	 *
	 * @param temperature the new temperature
	 */
	public void setTemperature(BigDecimal temperature) {
		this.temperature = temperature;
	}

	/**
	 * Gets the min temperature.
	 *
	 * @return the min temperature
	 */
	public BigDecimal getMinTemperature() {
		return minTemperature;
	}

	/**
	 * Sets the min temperature.
	 *
	 * @param minTemperature the new min temperature
	 */
	public void setMinTemperature(BigDecimal minTemperature) {
		this.minTemperature = minTemperature;
	}

	/**
	 * Gets the max temperature.
	 *
	 * @return the max temperature
	 */
	public BigDecimal getMaxTemperature() {
		return maxTemperature;
	}

	/**
	 * Sets the max temperature.
	 *
	 * @param maxTemperature the new max temperature
	 */
	public void setMaxTemperature(BigDecimal maxTemperature) {
		this.maxTemperature = maxTemperature;
	}

	/**
	 * Gets the sun set.
	 *
	 * @return the sun set
	 */
	public String getSunSet() {
		return sunSet;
	}

	/**
	 * Sets the sun set.
	 *
	 * @param sunSet the new sun set
	 */
	public void setSunSet(String sunSet) {
		this.sunSet = sunSet;
	}

	/**
	 * Gets the sun rise.
	 *
	 * @return the sun rise
	 */
	public String getSunRise() {
		return sunRise;
	}

	/**
	 * Sets the sun rise.
	 *
	 * @param sunRise the new sun rise
	 */
	public void setSunRise(String sunRise) {
		this.sunRise = sunRise;
	}

	/**
	 * Gets the timezone.
	 *
	 * @return the timezone
	 */
	public int getTimezone() {
		return timezone;
	}

	/**
	 * Sets the timezone.
	 *
	 * @param timezone the new timezone
	 */
	public void setTimezone(int timezone) {
		this.timezone = timezone;
	}

	/**
	 * Gets the customer id.
	 *
	 * @return the customer id
	 */
	public String getCustomerId() {
		return customerId;
	}

	/**
	 * Sets the customer id.
	 *
	 * @param customerId the new customer id
	 */
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
}

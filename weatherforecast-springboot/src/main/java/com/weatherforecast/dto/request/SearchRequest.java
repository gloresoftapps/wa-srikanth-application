package com.weatherforecast.dto.request;

import javax.validation.constraints.NotEmpty;

/**
 * The Class SearchRequest.
 */
public class SearchRequest {
	
	/** The name. */
	@NotEmpty(message="username must not be empty")
	private String name;
	
	/** The city. */
	@NotEmpty(message="city must not be empty")
	private String city;
	
	/** The customer id. */
	private String customerId;

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the city.
	 *
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Sets the city.
	 *
	 * @param city the new city
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * Gets the customer id.
	 *
	 * @return the customer id
	 */
	public String getCustomerId() {
		return customerId;
	}

	/**
	 * Sets the customer id.
	 *
	 * @param customerId the new customer id
	 */
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
}

package com.weatherforecast.dto.request;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

/**
 * The Class AccountRequest.
 */
public class AccountRequest {
	
	/** The username. */
	@Email(message="email is in bad format")	
	@NotEmpty(message="email must not be empty")
	private String username;
	
	/** The password. */
	@NotEmpty(message="password must not be empty")
	private String password;
	
	/** The date of birth. */
	@NotEmpty(message="dateOfBirth is invalid. Accepted format is yyyy-MM-d")
	private String dateOfBirth;
	
	/** The type. */
	@NotEmpty(message="type must not be empty. It should be either basic or admin")
	private String type;

	/**
	 * Gets the username.
	 *
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Sets the username.
	 *
	 * @param username the new username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the password.
	 *
	 * @param password the new password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Gets the date of birth.
	 *
	 * @return the date of birth
	 */
	public String getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * Sets the date of birth.
	 *
	 * @param dateOfBirth the new date of birth
	 */
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the type.
	 *
	 * @param type the new type
	 */
	public void setType(String type) {
		this.type = type;
	}	
}

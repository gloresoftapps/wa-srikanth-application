package com.weatherforecast.dto.request;

import java.util.List;

import javax.validation.constraints.NotEmpty;

/**
 * The Class DeleteRequest.
 */
public class DeleteRequest {

	/** The customer id. */
	@NotEmpty(message="customerId must not be empty")
	private String customerId;

	/** The weathers. */
	private List<String> weathers;

	/**
	 * Gets the customer id.
	 *
	 * @return the customer id
	 */
	public String getCustomerId() {
		return customerId;
	}

	/**
	 * Sets the customer id.
	 *
	 * @param customerId the new customer id
	 */
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	/**
	 * Gets the weathers.
	 *
	 * @return the weathers
	 */
	public List<String> getWeathers() {
		return weathers;
	}

	/**
	 * Sets the weathers.
	 *
	 * @param weathers the new weathers
	 */
	public void setWeathers(List<String> weathers) {
		this.weathers = weathers;
	}
}

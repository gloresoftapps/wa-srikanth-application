# Weatherforecast-Springboot

A spring boot web service to search and work with weather forecast and its information

# Technologies used
- Java 8
- Spring Boot 2.3.4
- REST
- Hibernate
- Postgres
- Maven 3
- Junit 4
- Swagger
- OpenPojo
- Sonar and Jacoco
- Spring Tool Suite 4 (IDE used for development)
- Postman (To validate API's)


## Project Setup
- Clone the project from following github link : `https://github.com/ksrikanth-github/weatherforecast-springboot.git`
- Install the following in your local environment 
  - Java 8 (`https://www.oracle.com/in/java/technologies/javase/javase-jdk8-downloads.html`)
  - Postgres database (`https://www.postgresql.org/download/windows/`)
  - pgAdmin(`https://www.pgadmin.org/download/pgadmin-4-windows/`)
  - Maven, Sonarlint, EclEmma Java Code Coverage, Moreunit  in Eclipse
- Import the project in IDE and then run Maven build.
- Navigate to src/main/resources folder and copy the DDL queries from weatherforecast_ddl_1/.sql file and create the tables in Postgres database. 
- Now we are ready to validate the API's in Postman
- The frontend has been integrated with Angular and here is the link to clone the frontend app : (`https://github.com/ksrikanth-github/weather-forecast-angular.git`)
- Once the API and Frontend is up and running in following ports 8080 and 4200 respectively the user shall register and start validating the application.

## Web Services
The following web services have been developed as part of the implementation. The endpoints mentioned are referring to local environment. 

For reading the latest weather updates, OpenWeatherMap API has been integrated for search functionality.

1. Register Customer.
2. Login Customer.
3. Search Weather By City Name.
4. Update Weather Details such as description, temperature, sunRise and sunSet.
5. Delete Weather
6. Delete Bulk Weather
7. Read Weather History for logged in Customer.
8. Search Customers By City Name
9. Search Cities By Customer Name 

API documentation is integrated with Swagger and the Swagger url can be accessed here : `http://localhost:8080/weatherforecast/api/swagger-ui.html`

Postman collection for API's can be found in the root folder of the project i.e in following link. Once the project is downloaded in your local, you can setup Postman and then import the json file to validate the API's.

## Further Enhancements/Improvements
- Improve coverage for code
- Add more unit test cases and also add integration test cases
- Exception handling and logger improvements
- Devops setup
- Configure secure data such as Database information, API token in secrets once the Devops pipeline is setup
- Migrate from Junit 4 to Junit 5 